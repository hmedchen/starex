module example.com/helmut/starex_vis_opengl

go 1.13

require (
	github.com/EngoEngine/math v1.0.4 // indirect
	github.com/engoengine/glm v0.0.0-20170725114841-9c08f4d1f668
	github.com/go-gl/gl v0.0.0-20210315015930-ae072cafe09d
	github.com/go-gl/glfw v0.0.0-20210410170116-ea3d685f79fb
)
