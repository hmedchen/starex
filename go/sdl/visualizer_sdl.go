package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"strconv"
	"time"

	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	ScreenWidth      = 1500
	ScreenHeight     = 500
	SW2              = 750
	SH2              = 250
	ScaleFactor      = 15.0
	FullRotationTime = 8.0
	CamX             = -20000
	CamY             = -20000
	CamZ             = 10000
)

type Coordinates struct {
	X int `json:"x"`
	Y int `json:"y"`
	Z int `json:"z"`
}

type Ecliptic struct {
	A int `json:"a"`
	B int `json:"b"`
}

type System struct {
	Coords   Coordinates `json:"coords"`
	Lum      float64     `json:"lum"`
	Colorstr string      `json:"color"`
	Color    sdl.Color
	Ecl      Ecliptic `json:"ecliptic"`
	//    Objects string
}

type Metadata struct {
	FileVersion int `json:"file_version"`
	NumSystems  int `json:"num_systems"`
	RandSeed    int `json:"rand_seed"`
}

func scan_file(filename string) string {
	// Open file and create scanner on top of it
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	// Scan for next token.
	success := scanner.Scan()
	if success == false {
		// False on error or EOF. Check error
		err = scanner.Err()
		if err == nil {
			log.Println("Scan completed and reached EOF")
		} else {
			log.Fatal(err)
		}
	}
	defer file.Close()
	return scanner.Text()
}

func getColor(s System) sdl.Color {
	r, _ := strconv.ParseInt(s.Colorstr[1:3], 16, 8)
	g, _ := strconv.ParseInt(s.Colorstr[3:5], 16, 8)
	b, _ := strconv.ParseInt(s.Colorstr[5:7], 16, 8)

	min := 10.0
	exponent := 0.25
	//exponent := 0.5
	//multiplicator := 20.0
	multiplicator := 30.0

	alpha := math.Pow(s.Lum, exponent) * multiplicator
	//alpha := s.Lum
	alpha = math.Max(alpha, min)
	//    alpha = uint8(math.Max(alpha, 255))

	mycol := sdl.Color(sdl.Color{uint8(r), uint8(g), uint8(b), uint8(math.Min(alpha, 255))})
	return mycol
}

func project(s System, cam Coordinates, angle_a float64, angle_b float64) Coordinates {
	//    cam := Coordinates{-20000,-20000,10000}
	//    var angle_a float64 = 1.5707963267
	//    var angle_b float64 = 2.68
	//    var focal_length float64 = 35.0
	var scale_factor = ScaleFactor
	//var scale_factor = 16.0

	// coords in relation to cam
	x := float64(s.Coords.X - cam.X)
	y := float64(s.Coords.Y - cam.Y)
	z := float64(s.Coords.Z - cam.Z)
	// distance to cam
	d_c := math.Sqrt(x*x + y*y + z*z)
	//    fmt.Println(d_c)
	//d_c := 30000.0
	// angles in relation to camera
	a_t := math.Atan(y / x)
	b_t := math.Asin(z / d_c)
	//    fmt.Println(d_c)
	//    b_t := math.Asin(z/d_c)
	//b_t := -0.3398369094541219
	a_c := a_t - angle_a
	b_c := b_t - angle_b
	// x,y in relation to cam position
	x_rel := math.Sin(a_c+a_t) * d_c
	y_rel := math.Sin(b_c+b_t) * d_c

	//    return (Coordinates{int(x_rel), int(y_rel), 0})
	return (Coordinates{int(x_rel / scale_factor), int(y_rel / scale_factor), 0})
}

func main() {
	var loopstart time.Time
	var fps int = 0
	var twopi = math.Pi * 2
	// init SDL
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		fmt.Println("initializing SDL", err)
		return
	}
	var x, y int32

	window, err := sdl.CreateWindow("Starex Starfield Visualisation",
		sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		ScreenWidth, ScreenHeight, sdl.WINDOW_OPENGL)
	if err != nil {
		fmt.Println("opening window", err)
		return
	}

	metatext := scan_file("../../saves/galaxy2/galaxy.meta")

	// Get data from scan with Bytes() or Text()
	var meta Metadata
	err = json.Unmarshal([]byte(metatext), &meta)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("File Version: %d, NumSystems %d, RandSeed %d\n", meta.FileVersion, meta.NumSystems, meta.RandSeed)
	fmt.Print("Loading Data...")
	start := time.Now()

	// start importing the Galaxywww
	galaxy := make([]System, meta.NumSystems)
	_ = galaxy
	file, err := os.Open("../../saves/galaxy2/galaxy.json")
	if err != nil {
		log.Fatal(err)
	}

	byteValue, err := ioutil.ReadAll(file)
	json.Unmarshal(byteValue, &galaxy)
	fmt.Printf("...done. (%d systems, %d ms)\n", len(galaxy), time.Since(start)/1000000)

	fmt.Printf("Preparing data...")
	for i := range galaxy {
		galaxy[i].Color = getColor(galaxy[i])
	}
	fmt.Printf("...done.\n")

	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Println("opening window", err)
		return
	}
	t1, err := renderer.CreateTexture(sdl.PIXELTYPE_UNKNOWN,
		sdl.TEXTUREACCESS_TARGET,
		ScreenWidth, ScreenHeight)
	/*
		t2, err := renderer.CreateTexture(sdl.PIXELTYPE_UNKNOWN,
			sdl.TEXTUREACCESS_TARGET,
			SW2, SH2)
		t3, err := renderer.CreateTexture(sdl.PIXELTYPE_UNKNOWN,
			sdl.TEXTUREACCESS_TARGET,
			int32(ScreenWidth/4), int32(ScreenHeight/4))
		_ = t2
		_ = t3
	*/
	//   fmt.Println(renderer.GetRenderTarget())
	renderer.SetRenderTarget(t1)
	//    fmt.Println(renderer.GetRenderTarget())
	// scales everything, including dots and ttf, so not working for me
	//renderer.SetScale(0.125,0.125)
	defer renderer.Destroy()
	//    var angle float64 = math.Pi/2

	// ----- MAINLOOP -----
	cam_pos := Coordinates{CamX, CamY, CamZ}
	//cam_pos := Coordinates{-20000, -20000, 10000}
	// calc distance of cam and degree of camera angle
	cam_dist := math.Sqrt(float64(cam_pos.X*cam_pos.X + cam_pos.Y*cam_pos.Y + cam_pos.Z*cam_pos.Z))
	fmt.Println(cam_dist)
	cam_pos_a := math.Atan(float64(cam_pos.Y) / float64(cam_pos.X))
	var angle_a float64 = 1.5707963267
	var angle_b float64 = 2.63
	//var angle_b float64 = 2.68
	var full_rot_time = FullRotationTime

	// a rotation should take 2 seconds
	rot := 0.02

	for {
		renderer.SetRenderTarget(nil)
		renderer.SetDrawColor(0, 0, 0, 0)
		renderer.Clear()
		renderer.SetRenderTarget(t1)
		t1.SetBlendMode(sdl.BLENDMODE_NONE)
		renderer.Clear()

		cam_pos.X = int(math.Cos(cam_pos_a) * cam_dist)
		cam_pos.Y = int(math.Sin(cam_pos_a) * cam_dist)

		loopstart = time.Now()
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				return
			}
		}

		var c Coordinates
		for i := 0; i < len(galaxy); i++ {
			c = project(galaxy[i], cam_pos, angle_a, angle_b)
			x = int32(c.X + SW2)
			y = int32(c.Y)
			gfx.PixelColor(renderer, x, y, galaxy[i].Color)
			//gfx.FilledCircleColor(renderer, x,y , 2, galaxy[i].Color)
		}

		elapsed := int(time.Since(loopstart))
		fps = 1000000000.0 / elapsed
		// copying the unscaled (t1) texture twice
		//        renderer.SetRenderTarget(t2)
		//       renderer.Copy(t1,nil,nil)
		//      renderer.SetRenderTarget(t3)
		//        renderer.Copy(t1,nil,nil)
		renderer.SetRenderTarget(nil)
		//renderer.Clear()
		//       t1.SetBlendMode(sdl.BLENDMODE_BLEND)
		//        t2.SetBlendMode(sdl.BLENDMODE_ADD)
		//       t3.SetBlendMode(sdl.BLENDMODE_ADD)
		//        t3.SetBlendMode(sdl.BLENDMODE_BLEND)
		//      renderer.Copy(t3,nil,nil)
		//     renderer.Copy(t2,nil,nil)
		renderer.Copy(t1, nil, nil)
		gfx.StringColor(renderer, ScreenWidth-32, 16, strconv.Itoa(fps), sdl.Color{255, 0, 0, 255})
		renderer.Present()

		_ = full_rot_time
		rot = (float64(elapsed) / 1000000000.0) * twopi / full_rot_time
		//        fmt.Println(rot, angle_a)

		//        fmt.Println(rot, float64(elapsed)/1000000000.0)

		cam_pos_a += rot
		if cam_pos_a > twopi {
			cam_pos_a -= twopi
		}

		angle_a += (2 * rot)
		if angle_a > twopi {
			angle_a -= twopi
		}

	}
}
