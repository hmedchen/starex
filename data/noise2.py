"""Writes a 256x256 grayscale simplex noise texture file in pgm format
(see http://netpbm.sourceforge.net/doc/pgm.html)
"""
# $Id: 2dtexture.py 21 2008-05-21 07:52:29Z casey.duncan $

import pygame
from pygame import gfxdraw
import sys
import os
from noise import pnoise2, snoise2

h=256
w=256
octaves =3
octaves2=3
d=16 # depth

pygame.display.init()
screen = pygame.display.set_mode((w+d,h+h+d))
freq = 16.0 * octaves
done = False

base1=130
base2=200

#h_plane = [w][d]
h_plane = [[0 for x in range(w)] for y in range(h)]
v_plane = [[0 for x in range(d)] for y in range(h)]
v2_plane = [[0 for x in range(w)] for y in range(d)]
print(len(v_plane), len(v_plane[0]))
#v_plane = [w][h]
while not done:
    for z in range(d):
        for x in range(w):
            try:
                v_plane[x][z]=int(pnoise2(x/freq, z/freq, octaves, base=base1) * 127.0 + 128.0)
                p=v_plane[x][z]
                gfxdraw.pixel(screen,x,h+h+z,pygame.Color(p,p,p))
            except IndexError:
                print (x,z)
                raise

    for z in range(h):
        for x in range(d):
            try:
                v2_plane[x][z]=int(pnoise2(x/freq, z/freq, octaves, base=base2) * 127.0 + 128.0)
                p=v2_plane[x][z]
                gfxdraw.pixel(screen,x+w,h+z,pygame.Color(p,p,p))
            except IndexError:
                print (x,z)
                raise

    for y in range(h):
        for x in range(w):
            try:
                h_plane[x][y] = int(pnoise2(x/freq, y/freq, octaves2) * 127.0 + 128.0)
                p=h_plane[x][y]
                gfxdraw.pixel(screen,x,h+y,pygame.Color(p,p,p))
            except IndexError:
                print (x,y)
                raise

            #s=int(snoise2(x/freq, y/freq, octaves) * 127.0 + 128.0)
    #        p=int(pnoise2(x/freq, y/freq, octaves) * 127.0 + 128.0)

    for i in range(d):
#        screen.fill((0,0,0))
        pygame.draw.rect(screen,(0,0,0),(0,0,x,y))
        for y in range(h):
            for x in range(w):
                p = (h_plane[x][y] + (v_plane[y][i] + v2_plane[i][x])//2)//2
#                gfxdraw.pixel(screen,x,y,(p,p,p))
                if p < 118:
##                    gfxdraw.pixel(screen,x,y,(p,p,p))
                    gfxdraw.pixel(screen,x,y,(255,255,255))
        #gfxdraw.pixel(screen,x+w,y,pygame.Color(s,s,s))

        pygame.display.flip()
        pygame.time.wait(10)

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_q):
            done = True
print ("bla")
pygame.display.quit()
#pygame.quit()
sys.exit()
#os._exit()
