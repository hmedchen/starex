#!/usr/bin/env python3
import sys
import os
import argparse
import logging
import logging.handlers
import json

from starex.game import Game

#import client.cli
from starex.client import client
from starex.client.terminal_client import terminal_gui
from starex.client.pygame_client import pygame_gui

#from cfg.server import server_config

from starex.server.server import RPCServer
from starex.server.server import LocalServer
from starex import configreader


def get_config():
    ''' evaluating command line arguments and read config'''
    # read standard config
#    config = server_config.config
    cr = configreader.ConfigReader(
        os.path.join("cfg","server"),"server.cfg")
    config = cr.config
    print (config)
#    with open("cfg/server/server_config.py","r") as fp:
#        config = json.load(fp)
    # read cmd line args
    parser = argparse.ArgumentParser(
        description="The Starex server process. \
                     The command line arguments are still wip",
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "-i", "--ip", type=str, default=config['connection']['host'], dest='host',
        action="store", help="IP/hostname to listen at")
    parser.add_argument(
        "-p", "--port", type=int, default=config['connection']['port'],
        action="store", help="port to listen on")
    parser.add_argument(
        "-d", "--daemon",
        action="store_true", help="run in background")
    parser.add_argument(
        "-s", "--socket",
        action="store_true", help="listen on linux socket")
    parser.add_argument(
        "-t", "--testrun",
        action="store_true", help="runs a set of tests")
    parser.add_argument(
        "-S", "--server",
        action="store_true", help="start standalone server")
    parser.add_argument(
        "-C", "--client",
        action="store_true", help="start standalone client")
    parser.add_argument(
        "-v", "--verbose", dest='debug',
        action="store_true", help="print debug info")
    args = parser.parse_args()

    # Getting the base path of the server.
    #config['basepath'] = os.path.abspath(os.path.join(os.curdir, sys.argv[0],
    #                                     os.pardir))

    config['basepath'] = os.path.abspath(os.path.join(os.curdir))
    # Merge cmd line args into config - NO SANITY CHECKING!
    return {**config, **vars(args)}


def daemonize(logger, config):
    ''' starts the server as daemon if requested '''
    logger.info("Starting daemon")
    try:
        fork_pid = os.fork()
    except OSError as e:
        logger.critical("Fork failed. {0} {1}\n".format(e.strerror, e.errno))
        sys.exit(1)

    if fork_pid > 0:
        sys.exit(0)
    else:
        config['pid'] = os.getpid()
        logger.info("Forked process has PID " + str(config['pid']))

        config['pidfile'] = os.path.abspath(
                os.path.join(config['basepath'],
                             config['logging']['logdir'],
                             config['srv']['pidfile_name']))

        if os.path.isfile(config['pidfile']):
            logger.error("PID File" + config['pidfile'] +
                         "already exists. Aborting")
            sys.exit(2)
        else:
            with open(config['pidfile'], 'w') as pf:
                pf.write(str(config['pid']) + "\n")


def init_logging(config):
    '''
    initialize logging
    'logging_file' and 'logging_stdout' in the config determines where the
    logging goes to
    '''
    logger = logging.getLogger('Starex')
    # clearing existing handlers to prevent dupe logging
    if logger.hasHandlers():
        logger.handlers.clear()

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    # config file logging handler
    # create directory if needed
    logdir = os.path.abspath(
            os.path.join(config['basepath'], config['logging']['logdir']))
    if os.path.isdir(logdir) is False:
        print("Log dir does not exist, trying to create")
        try:
            os.mkdir(logdir)
        except OSError:
            print("FATAL: cannot create log directory (%s)- exitting"
                  % (logdir))
            sys.exit(1)

    if config['server'] is False:
        # turn daemon mode off for standalone game
        config['daemon'] = False
        logger.debug("Switching daemon mode off for standalone game")

    if config['daemon'] is True or config['server'] is False:
        if config['logging']['logging_stdout']:
            config['logging']['logging_stdout'] = False
            config['logging']['logging_file'] = True
            logger.debug("Switching logging to file for deamon mode")

    if config['logging']['logging_file']:
        #fh = logging.FileHandler(os.path.join(logdir, config['logfile_name']))
        fh = logging.handlers.TimedRotatingFileHandler(
            os.path.join(logdir, config['logging']['logfile_name']),
            when='D', backupCount=3)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    if config['logging']['logging_stdout']:
        sh = logging.StreamHandler(stream=sys.stdout)
        sh.setFormatter(formatter)
        logger.addHandler(sh)
    # set logging level (DEBUG->INFO->WARNING->ERROR->CRITICAL)
    if config['debug']:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    return logger


# ------------------------------------------------------
# MAIN
# ------------------------------------------------------
def main():
    '''
    main loop of the starex RPC server
    '''
    # Getting cmd line args and read config
    config = get_config()

    logger = init_logging(config)

    logger.info("----------------------------------------------------------")
#    logger.info(" Starting Server....")

#    logger.debug("Configuration: " + str(config))
    if config['daemon']:
        daemonize(logger, config)

    if config['server']:
        logger.debug("Server Only")
        game = Game(config, RPCServer, None)
        logger.info("Starting Standalone Server....")
        logger.debug("Start serving")
        game.serve()
        game.exit("Exitting. Goodbye.")
    elif config['client']:
        logger.info("Starting Standalone Client....")
#        game_client = client.Client()
        game_client = terminal_gui.asciiclient()
        #game = Game(config, LocalServer, terminal_gui.asciiclient)
        logger.debug("Starting client main loop")
        try:
            game_client.mainloop()
        except ConnectionRefusedError:
            logger.error("Cannot connect to server. exitting.")
            print ("Cannot connect to server. exitting.")
            sys.exit(1)
    elif config['testrun']:
        logger.info("Starting local test run")
        game = Game(config, LocalServer, client.Client)
        game.start()
        logger.debug("Starting tests")
        game.client.mainloop()
        game.exit(0, "Exitting. Goodbye.")
    else:
        logger.info("Starting local full game")
#        game = Game(config, LocalServer, terminal_gui.asciiclient)
        game = Game(config, LocalServer, pygame_gui.GFXClient)
        game.start()
        logger.debug("Starting client main loop")
        game.client.mainloop()
        game.exit(0, "Exitting. Goodbye.")


if __name__ == "__main__":
    main()
