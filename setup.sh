#!/usr/bin/env bash
virtualenv --python=python3 ~/venv_starex
. ~/venv_starex/bin/activate && pip install -r venv_requirements.txt
deactivate
