import sys
import json
import os
import logging

from configparser import ConfigParser


class ConfigReader(object):
    def __init__(self, filepath=None, file="noname.cfg"):
        self.config={}
        self.logger = logging.getLogger("Starex")
        if filepath is None:
            filepath = "."
        self.config_path = os.path.join(filepath, file)
        self.configfile = ConfigParser(allow_no_value=True)

        try:
            self.configfile.read_file(open(self.config_path))
        except:
            self.logger.error("Cannot open config file.")

        self.parse()


    def _set_bool_from_str(self, value):
        if value in ("True", "true", "yes", "Yes", "1"):
            return True
        else:
            return False

    def parse(self):
        for section in self.configfile.sections():
            self.config.update({section: {} })
            for item in self.configfile.items(section):
#                self.config.update({[section][item]: self.configfile.get(section,item) })
                if item[1][0] in ("[","("):
                    itemval = json.loads(item[1])
                elif item[1][0].isalpha() or item[1][0] == "#":
                    itemval = item[1]
                elif "." in item[1]:
                    itemval = float(item[1])
                else:
                    itemval = int(item[1])
                self.config[section].update({item[0]: itemval})


if __name__ == "__main__":
    c = ConfigReader(os.path.join("..","cfg","client"),"client.cfg")

    print (c.config)
