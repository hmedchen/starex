# pygame imports
import os
import json
import logging

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import pygame
import math
import cv2

from ..celestialobjects import galaxy_config
from .. import configreader
#import image_config
#import server_config


class Canvas:
    ''' base class for a surface to draw on '''
    def __init__(self, basedir, savepath, size, screen, scale):
        self.basepath = basedir
        self.savepath = savepath
        self.cfgpath = os.path.join(basedir,"cfg","server")
        self.width = size[0]
        self.height = size[1]
        self.screen = pygame.Surface((self.width, self.height),
                                     pygame.SRCALPHA)
        self.centerx = self.width // 2
        self.centery = self.height // 2
        self.scale = scale

        self.configreader = configreader.ConfigReader(
            os.path.join("cfg","server"),"visualizer.cfg")
        self.config = self.configreader.config

        self.seed = None
        self.clear()

    def clear(self):
        self.screen.fill((0,0,0))

    def drawDot(self, paintx, painty, System):
        '''
        Draw one dot on the surface on given x/y coords.
        This method handles the color and alpha of the dot
        '''
        paintx=int(paintx)
        painty=int(painty)
        if (paintx > 0 and paintx <= self.width
                and painty > 0 and painty <= self.height):
            s = pygame.Surface((1, 1), flags=pygame.SRCALPHA)
            color_alpha = int(math.pow(System.get_lum(),
                              self.config['alpha']['exponent']) *
                              self.config['alpha']['multiplicator'])
#            print (System.get_lum(), color_alpha)
            if color_alpha <= self.config['alpha']['min']:
                color_alpha = self.config['alpha']['min']
            elif color_alpha > 255:
                color_alpha = 255
            try:
                dot_color = pygame.Color(System.get_color())
            except ValueError:
                print (System.get_color())
                raise
            dot_color.a = color_alpha
#            print (dot_color)
            s.fill(dot_color)
            self.screen.blit(s, (paintx, painty))
            #self.screen.blit(s, (paintx, painty), special_flags=pygame.BLEND_ADD)

    def saveToFile(self, filename='dummy.png'):
        '''
        Saves the finshed image to disk.
        Also blurs the pictfor a better effect
        '''
        self.blur()
#        imgdir = os.path.join(self.basepath,

#                              self.config['files']['img_path'])
        if os.path.isdir(self.savepath) is False:
            print("Img dir does not exist, trying to create")
            try:
                os.mkdir(self.savepath)
            except OSError:
                print("FATAL: cannot create img directory (%s)- exitting"
                      % (self.savepath))
                return False
        full_filename = os.path.join(self.savepath, filename)
#        print (full_filename)
        pygame.image.save(self.screen, full_filename)
        return True

    def blur(self):
#        print ("blurring")
        ''' Blurs the finished image for better effect '''
        for scale in self.config['image']['gauss_filter_values']:
            scale_size = (int(self.width/scale), int(self.height/scale))
            surf_small = pygame.transform.smoothscale(self.screen, scale_size)
            surf = pygame.transform.smoothscale(surf_small,
                                                (self.width, self.height))
            self.screen.blit(surf, (0,0), special_flags=pygame.BLEND_RGBA_MAX)
            #self.screen.blit(surf, (self.width, self.height))


class Canvas3d(Canvas):
#    def __init__(self, size, screen, scale):
#        super(Canvas3d, self).__init__(size, screen, scale)
#        self.a_c = 0.0
#        self.b_c = 0.0

    ''' Canvas child for 3d view. '''
    def paintSystem(self, star, animation=False):
        ''' draws a 3D representation of the Galaxy '''
        # coordinates in relation to Camera
        x_t = star.coords.x - self.config['camera']['x']
        y_t = star.coords.y - self.config['camera']['y']
        z_t = star.coords.z - self.config['camera']['z']
        # distance to camera
        d_c = math.sqrt(x_t * x_t + y_t * y_t + z_t * z_t)
        # angles in relation to camera
        a_t = math.atan(y_t / x_t)
        # dirty hack to get the angles right (where's the bug?)
        if animation:
            b_t = math.asin(z_t / d_c)
            # camera angles
            a_c = a_t - self.config['camera']['a']
            b_c = b_t - self.config['camera']['b']
        else:
            b_t = math.atan(z_t / x_t)
            # camera angles
            a_c = a_t - self.config['camera']['a']
            b_c = b_t - self.config['camera']['b']
        # x,y in relation to cam position
        #x_rel = math.sin(a_c + a_c) * d_c
        x_rel = math.sin(a_c + a_t) * d_c
        y_rel = math.sin(b_c + b_c) * d_c

        paintx = x_rel // self.scale // 2 + self.centerx
        painty = y_rel // self.scale // 2 + self.centery
        self.drawDot(paintx, painty, star)

    def save(self):
        ''' sets the file name and saves '''
        FileName = self.config['files']['img_name_view_3d']
        self.saveToFile(FileName)

    def get_cam(self):
        return [[self.config['camera']['x'],
                 self.config['camera']['y'],
                 self.config['camera']['z']],
                [self.config['camera']['a'],
                 self.config['camera']['b']]]

    def set_cam(self, x,y,z,a,b):
        self.config['camera']['x']=x
        self.config['camera']['y']=y
        self.config['camera']['z']=z
        self.config['camera']['a']=a
        self.config['camera']['b']=b


class CanvasX(Canvas):
    ''' Canvas child for X view. '''
    def paintSystem(self, star):
        ''' draws the X view of the Galaxy '''
        paintx = ((star.coords.x / self.scale) + self.centerx)
        painty = ((star.coords.z / self.scale) + self.centery)
        self.drawDot(paintx, painty, star)

    def save(self):
        ''' sets the file name and saves '''
        FileName = self.config['files']['img_name_view_x']
        self.saveToFile(FileName)


class CanvasY(Canvas):
    ''' Canvas child for Y view. '''
    def paintSystem(self, star):
        ''' draws the Y view of the Galaxy '''
        paintx = ((-star.coords.y / self.scale) + self.centerx)
        painty = ((star.coords.z / self.scale) + self.centery)
        self.drawDot(paintx, painty, star)

    def save(self):
        ''' sets the file name and saves '''
        FileName = self.config['files']['img_name_view_y']
        self.saveToFile(FileName)


class CanvasZ(Canvas):
    ''' Canvas child for Z view. '''
    def paintSystem(self, star):
        ''' draws the Z view of the Galaxy '''
        paintx = ((star.coords.x / self.scale) + self.centerx)
        painty = ((star.coords.y / self.scale) + self.centery)
        self.drawDot(paintx, painty, star)

    def save(self):
        ''' sets the file name and saves '''
        FileName = self.config['files']['img_name_view_z']
        self.saveToFile(FileName)


class Visualizer:
    ''' wrapper class to create the 4 perspective images '''
    def __init__(self, basedir, savepath):
        ''' setting up the 4 perspective images '''
        self.logger = logging.getLogger('Starex')
        self.basepath = basedir
        self.savepath = savepath
        self.cfgpath = os.path.join(self.basepath,"cfg","server")
    #    with open("../../cfg/server/image.config.py","r") as fp:
        self.configreader = configreader.ConfigReader(
            os.path.join("cfg","server"),"visualizer.cfg")
        self.config = self.configreader.config
        #with open(os.path.join(self.cfgpath,"image_config.py"),"r") as fp:
        #    self.config = json.load(fp)
        # init screen
        self.size = [self.config['image']['width'],
                     self.config['image']['height']]

        try:
            pygame.init()
            self.screen = pygame.Surface(self.size, flags=pygame.SRCALPHA)
        except Exception as e:
            print(e)
            raise

        self.ViewX = CanvasX(
            self.basepath,
            self.savepath,
            self.size, self.screen,
            galaxy_config.galaxy_conf['dimensions']['diameter'] /
            self.config['image']['width'] *
            self.config['image']['scale_factor'])
        self.ViewY = CanvasY(
            self.basepath,
            self.savepath,
            self.size, self.screen,
            galaxy_config.galaxy_conf['dimensions']['diameter'] /
            self.config['image']['width'] *
            self.config['image']['scale_factor'])
        self.ViewZ = CanvasZ(
            self.basepath,
            self.savepath,
            self.size, self.screen,
            galaxy_config.galaxy_conf['dimensions']['diameter'] /
            self.config['image']['width'] *
            self.config['image']['scale_factor'])
        self.View3D = Canvas3d(
            self.basepath,
            self.savepath,
            self.size, self.screen,
            galaxy_config.galaxy_conf['dimensions']['diameter'] /
            self.config['image']['width'] *
            self.config['image']['scale_factor'])

    def save_galaxy_images(self, Galaxy):
        '''
        painting all systems of the galaxy in 4 dimensional images
        and saving these images when finished.
        '''
        for i in range(0, Galaxy.num_systems):
            sys = Galaxy.systems[i]
            try:
                self.ViewX.paintSystem(sys)
                self.ViewY.paintSystem(sys)
                self.ViewZ.paintSystem(sys)
                self.View3D.paintSystem(sys)
            except IndexError:
                print("error: unable to get system at index %s" % i)
        self.ViewX.save()
        self.ViewY.save()
        self.ViewZ.save()
        self.View3D.save()

    def save_galaxy_animation(self, Galaxy):
        self.logger.debug("saving Animation...")
        self.ViewANI = Canvas3d(
            self.basepath,
            self.savepath,
            [self.config['animation']['width'],self.config['animation']['height']],
            self.screen,
            galaxy_config.galaxy_conf['dimensions']['diameter'] /
            self.config['animation']['width'] *
            self.config['animation']['scale_factor'])

        steps = self.config['animation']['fps']* self.config['animation']['seconds']
        # shortcut
        cam = self.config['camera_ani']
        self.logger.debug("{:d}s of {:d} fps video ({:d}:{:d})".format(
                            self.config['animation']['seconds'],
                            self.config['animation']['fps'],
                            self.config['animation']['width'],
                            self.config['animation']['height'] ))

        # by how much of a radiant do we rotate
        rot_a = 2*math.pi/steps

        # calc distance of cam and degree of camera angle
        cam_dist = math.sqrt(cam['x']**2 + cam['y']**2)
        cam_pos_a = math.atan(cam['y']/cam['x'])

        for i in range(steps):
            print(i)
            self.ViewANI.clear()
            for n in range (0, Galaxy.num_systems):
                sys = Galaxy.systems[n]
                self.ViewANI.paintSystem(sys, True)
            self.ViewANI.saveToFile("ani_{:04d}_frm.png".format(i))

            cam_pos_a += rot_a

            [x,y,z],[a,b]=self.ViewANI.get_cam()


            a += (2*rot_a)
            x = math.cos(cam_pos_a) * cam_dist
            y = math.sin(cam_pos_a) * cam_dist

            self.ViewANI.set_cam(x,y,z,a,cam['b'])



        video_name = os.path.join(self.savepath, 'galaxy.avi')
        images = [img for img in os.listdir(self.savepath) if img.endswith("frm.png")]
        images.sort()

        frame = cv2.imread(os.path.join(self.savepath, images[0]))
        height, width, layers = frame.shape
#        print(frame.shape)

        video = cv2.VideoWriter(video_name, 0, self.config['animation']['fps'],
                                (width,height))

        for image in images:
            video.write(cv2.imread(os.path.join(self.savepath, image)))

        video.release()

        #cleanup:
        for image in images:
            os.remove(os.path.join(self.savepath, image))

        self.logger.debug("saving Animation done.")
