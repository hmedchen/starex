from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import socket


class LocalServer:
    '''
    Class that starts the local server,
    defines and registers the functions.
    Class mimics the RPCServer class, so the client can be used remotely
    and locally combined with the server.
    '''

    def __init__(self, mainfunc, config, logger):
        self.config = config
        self.logger = logger
        self.mainfunc = mainfunc
        self.logger.info("Local server started")
        self.funcs = {}
#        self.poll_interval = self.config['poll_interval']

    def register(self, func):
        '''
        registering the usable server functions.
        creating a function, renaming it accordingly and assigning
        it to the class
        '''
        fnname = func.__name__

        def fn(self, request):
            logstr = "Function " + str(fnname) + " called."
            self.logger.debug(logstr)
            myreply = self.funcs[fnname](request)
            return myreply.strip()

        setattr(LocalServer, fnname, fn)
        self.funcs[fnname] = func

    def authenticate(self, auth):
        ''' Stub '''
        return True


class RPCServer:
    '''
    Class that starts the RPC server, defines and registers the functions
    '''

    def __init__(self, mainfunc, config, logger):
        self.config = config
        self.logger = logger
        self.mainfunc = mainfunc
        # This is neccessary to pass the logger to the class

        def handler(*args):
            try:
                RequestHandler(logger, *args)
            except BrokenPipeError:
                self.logger.warning("Broken pipe when handling " + str(args))

        # get host
        if self.config['host'] is None:
            self.config['host'] = socket.gethostname()
        # Create server
        self.server = MyXMLRPCServer(
            self.mainfunc,
            (self.config['host'], self.config['port']), requestHandler=handler)
        self.server.register_introspection_functions()

        self.logger.info("listening on "
                         + self.config['host']
                         + ":" + str(self.config['port']))

    def serve_forever(self, poll_interval):
        ''' main loop, interrupted regularly for internal clls '''
        self.server.serve_forever(self.config['poll_interval'])

    def register(self, func):
        ''' Registering the server functions '''
        self.server.register_function(func)

    def authenticate(self, auth):
        ''' Stub '''
        return True


class RequestHandler(SimpleXMLRPCRequestHandler):
    '''
    Subclass of the SimpleXMLRPCRequestHandler
    'logger' is passed to it on __init__, therfore the  logger has to be
    created using a function like this:
        def handler(*args):
            RequestHandler(logger,*args)
    '''
    rpc_paths = ('/RPC2',)

    def __init__(self, logger,  request, client_address, server):
        # pass the logger
        self.logger = logger
        super().__init__(request, client_address, server)

    def log_error(self, format, *args):
        self.looger.error("error:", format, args)

    def log_message(self, format, *args):
        self.logger.info("%s - - [%s] %s" %
                         (self.address_string(),
                          self.log_date_time_string(),
                          format % args))


class MyXMLRPCServer(SimpleXMLRPCServer):
    def __init__(self, mainfunc, configuration, requestHandler):
        self.mainfunc = mainfunc
        super().__init__(configuration, requestHandler=requestHandler)

    def service_actions(self):
        '''
        is called roughly every 0.5s (default)
        this can be adjusted in the serve_forever call, and with the
        config['poll_interval'] variable
        '''
        self.mainfunc()
        super().service_actions()
