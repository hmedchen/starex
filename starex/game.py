# common imports
import signal
import sys
import os
import json
import time
import logging
import platform
from threading import Timer

# project imports
from .celestialobjects import galaxy
from .server import visualizer

rpc_reply = {
    'success': False,
    'elapsed': 0,
    'errno': 0,
    'errmsg': '',
    'payload': None,
    'completed': True,   # for async requests
    'etc': 0}            # Estimated time for completion

class Game:
    ''' main class of the game, handling all the timing and main loop '''

    def __init__(self, config, server, client):
        ''' constructor, catching signals and registering rpc functions '''
        self.config = config
#        self.logger = logger
        self.logger = logging.getLogger("Starex")
        self.clientObj = client
        self.client = None

        self.server = server(self.mainfunc, self.config, self.logger)

        self.logger.debug("initializing")
        self.os = platform.system()
        # initialize signal
        signal.signal(signal.SIGTERM, self.catch_signal)
        signal.signal(signal.SIGINT, self.catch_signal)
        signal.signal(signal.SIGABRT, self.catch_signal)

        # functions to register with the RPC server
        self.server.register(self.logo)
        self.server.register(self.create_galaxy)
        self.server.register(self.save_images)
        self.server.register(self.save_animation)
        self.server.register(self.save_galaxy)
        self.server.register(self.list_galaxies)
        self.server.register(self.delete_galaxy)
        self.server.register(self.load_galaxy)
        self.server.register(self.random_system)
        self.server.register(self.systems_in_range)

        self.galaxy = galaxy.Galaxy(self.config, self.logger)

        self.timer = None
        self.start_timer(self.mainfunc)

    def exit(self, retval=0, message=''):
        ''' handling the exit statements. sys.exit() should not be used '''
        if self.config['daemon']:
            os.remove(self.config['pidfile'])
        self.logger.info(message)
        os._exit(retval)

    def catch_signal(self, sig, stack):
        ''' makes sure the server exits gracefully when killed '''
        if sig == signal.SIGINT:
            self.exit(0, "Received SIGINT - Exitting.")
        elif sig == signal.SIGTERM:
            self.exit(10, "Received SIGTERM - Exitting")
        else:
            self.exit(20, "Unknown Signal - Exitting")
        self.exit(100)

    def start_timer(self, funcname):
        self.timer = Timer(self.config['srv']['poll_interval'], funcname)
        self.timer.start()

    def mainfunc(self, sig=None, stack=None):
        '''
        This function is called every rpc loop time.
        AI logic should go here
        '''
        self.start_timer(self.mainfunc)

    def start(self):
        self.logger.debug("starting client")
        self.client = self.clientObj(self.server, self)
#        self.client.main(self.server)
        self.logger.debug("...done")

    def serve(self):
        ''' starts the game rpc server '''
        self.server.serve_forever(self.config['srv']['poll_interval'])

    def logo(self, req):
        '''
        RPC call to return the Starex logo
        This is mainly used for debugging purposes

        Returns a json.dumps() packed string of 'server_config.rpc_reply'

        Significant fields:
        - 'success': [True/False]
            - 'errno': error number, or 'None'
            - 'errmsg': error message, or 'None'
        - 'payload': A string list containing the logo or 'None'
        '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            with open(os.path.join(self.config['basepath'],
                                   self.config['locations']['logo_loc']), 'r') as fp:
                logo = fp.readlines()
            if logo is not None:
                reply['success'] = True
                reply['payload'] = logo
                reply['elapsed'] = int((time.time() - starttime) * 1000)
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        return json.dumps(reply)

    def create_galaxy(self, req, seed=None):
        '''
        Creating the galxy in a predetermined pattern.
        This might be changed to template based creations in the future.
        '''
        self.logger.info("Creating new galaxy..")
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        self.galaxy.clear()
        self.galaxy.seed(seed)
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            num_stars = request['payload']
            self.logger.debug(num_stars)
            time1 = self.galaxy.add_disc(0.25, 1, num_stars // 3)
            time2 = self.galaxy.add_disc(0.5, 0.25, num_stars // 6)
            time3 = self.galaxy.add_arms(0.5, 1, num_stars // 3, 2)
            time4 = self.galaxy.add_arms(0.5, 1, num_stars // 6, 4)
            self.logger.debug("timings: " + str(time1) + ", "
                              + str(time2) + ", "
                              + str(time3) + ", "
                              + str(time4))
            totaltime = time1 + time2 + time3 + time4
            self.logger.debug("count:" + str(len(self.galaxy.systems)))
            self.logger.debug("sum: " + str(totaltime)
                              + " = " + str(num_stars // totaltime // 1000)
                              + "systems/ms")
            reply['success'] = True
            reply['payload'] = {'num_stars': num_stars,
                                'num_collisions': self.galaxy.num_collisions}
            reply['elapsed'] = int((time.time() - starttime) * 1000)
            starttime = time.time()
            self.logger.debug("Creating kdtree")
            self.galaxy.tree_create()
            self.logger.debug("created in " + str(int(time.time()-starttime)))
            self.logger.debug("getting random systems")
            self.galaxy.create_sys_objects()
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        self.logger.info("Galaxy with {:d} systems created in {:d}s.".format(num_stars, reply['elapsed']))
        return json.dumps(reply)

    def save_animation(self,req):
        ''' saves a rotating galaxy movie to disk '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        try:
           gal_name = request['payload']
        except TypeError:
            self.logger.error("Error save_images: no name provided")
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            dirname = os.path.join(self.config['basepath'],
                                   self.config['locations']['savedir'],
                                   gal_name)
            v = visualizer.Visualizer(self.config['basepath'],dirname)
            v.save_galaxy_animation(self.galaxy)
            reply['success'] = True
            reply['elapsed'] = int((time.time() - starttime) * 1000)
            # -------------------------

        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        reply_j = json.dumps(reply)
        return reply_j

    def save_images(self, req):
        ''' saves the galaxy images to disk '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        try:
           gal_name = request['payload']
        except TypeError:
            self.logger.error("Error save_images: no name provided")
        print (gal_name)
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            dirname = os.path.join(self.config['basepath'],
                                   self.config['locations']['savedir'],
                                   gal_name)
            v = visualizer.Visualizer(self.config['basepath'], dirname)
            #v = visualizer.Visualizer(self.config['basepath'])
            v.save_galaxy_images(self.galaxy)
            reply['success'] = True
            reply['elapsed'] = int((time.time() - starttime) * 1000)
            # -------------------------

        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        reply_j = json.dumps(reply)
        return reply_j
#        return json.dumps(reply)

    def random_system(self, req):
        ''' return a random system '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            reply['payload'] = self.galaxy.random_system()
            reply['success'] = True
            reply['elapsed'] = int((time.time() - starttime) * 1000)
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        return json.dumps(reply)

    def systems_in_range(self, req):
        ''' return a random system '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
#        print (request)
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            #print("a")
            coords = [request['payload']['coords']['x'],
                      request['payload']['coords']['y'],
                      request['payload']['coords']['z']]
            #print("b")
            dist = request['payload']['range']
            ##print(coords, dist)
            #print(self.galaxy.tree.search_nn_dist(coords, dist**2))
            reply['payload'] = 'bla'
            reply['success'] = True
            reply['elapsed'] = int((time.time() - starttime) * 10000)
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        return json.dumps(reply)

    def delete_galaxy(self, req):
        pass

    def list_galaxies(self, req):
        '''lists all galaxies saved '''
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        if self.server.authenticate(request['auth']):
            dirname = os.path.join(self.config['basepath'],
                                   self.config['locations']['savedir'])
            allsaves = os.scandir(dirname)
            saves={"saves":[]}
            for entry in allsaves:
                if entry.is_dir():
                   saves["saves"].append({"name": entry.name,
                                 "created": entry.stat().st_ctime,
                                 "last updated": entry.stat().st_mtime})
            reply['success'] = True
            reply['payload'] = saves
            reply['elapsed'] = int((time.time() - starttime) * 1000)
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        print (reply)
        return json.dumps(reply)

    def save_galaxy(self, req):
        ''' saves the complete galaxy into json format '''
        self.logger.info("Saving Galaxy.")
        starttime = time.time()
        request = json.loads(req)
        print (request)
        reply = rpc_reply
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            try:
                gal_name = request['payload']
            except TypeError:
                self.logger.error("Error save_galaxy: no name provided")
            dirname = os.path.join(self.config['basepath'],
                                   self.config['locations']['savedir'],
                                   gal_name)
#            filename = "galaxy_objects.dat"
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            self.galaxy.save(os.path.join(dirname))
            #self.galaxy.save(os.path.join(dirname,filename))
            reply['success'] = True
            reply['payload'] = {'save_path': dirname}
            reply['elapsed'] = int((time.time() - starttime) * 1000)
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        self.logger.info("Galaxy {:s} saved.".format(gal_name))
        return json.dumps(reply)

    def load_galaxy(self, req):
        ''' loads the complete galaxy from json file '''
        self.logger.info("Loading Galaxy...")
        starttime = time.time()
        request = json.loads(req)
        reply = rpc_reply
        if self.server.authenticate(request['auth']):
            # --------- real code -----
            gal_name = request['payload']
            dirname = os.path.join(self.config['basepath'],
                                    self.config['locations']['savedir'],
                                    gal_name)
            retval = self.galaxy.load(dirname)
            if retval == False:
                reply['success'] = False
                reply['errno'] = 110
                reply['errmsg'] = 'File not Found'
            else:
                reply['success'] = True
            reply['payload'] = {'load_path': dirname}
            reply['elapsed'] = int((time.time() - starttime) * 1000)
    #        self.logger.debug("Creating kdtree")
    #        self.galaxy.tree_create()
            self.logger.debug(
                "created in " + str(int(time.time() - starttime)))
            # -------------------------
        else:
            reply['errno'] = 100
            reply['errmsg'] = 'Authentication Failed'
        self.logger.info("Galaxy {:s} loaded.".format(gal_name))
        return json.dumps(reply)
