symbols = {
    "M8": { "sym": "b", "desc": "Brown Dwarf"},
    "M9": {"sym": "b", "desc": "Brown Dwarf"},
    "L0": {"sym": "b", "desc": "Brown Dwarf"},
    "L2": {"sym": "b", "desc": "Brown Dwarf"},
    "L5": {"sym": "b", "desc": "Brown Dwarf"},
    "T0": {"sym": "b", "desc": "Brown Dwarf"},
    "T5": {"sym": "b", "desc": "Brown Dwarf"},
    "T8": {"sym": "b", "desc": "Brown Dwarf"},
    "O3": {"sym": "}", "desc": "Supergiant Star"},
    "O8": {"sym": "}", "desc": "Supergiant Star"},
    "pMSi": {"sym": "o", "desc": "Planet"},
    "pSi": {"sym": "o", "desc": "Planet"},
    "pHSi": {"sym": "o", "desc": "Planet"},
    "pC": {"sym": "o", "desc": "Planet"},
    "pH2O": {"sym": "o", "desc": "Planet"},
    "gH": {"sym": "O", "desc": "Planet"},
    "gHe": {"sym": "O", "desc": "Planet"},
    "gCO": {"sym": "O", "desc": "Planet"},
    "B0": {"sym": ")", "desc": "Main Sequence Stars"},
    "B3": {"sym": ")", "desc": "Main Sequence Stars"},
    "B5": {"sym": ")", "desc": "Main Sequence Stars"},
    "B8": {"sym": ")", "desc": "Main Sequence Stars"},
    "A0": {"sym": ")", "desc": "Main Sequence Stars"},
    "A5": {"sym": ")", "desc": "Main Sequence Stars"},
    "F0": {"sym": ")", "desc": "Main Sequence Stars"},
    "F5": {"sym": ")", "desc": "Main Sequence Stars"},
    "G0": {"sym": ")", "desc": "Main Sequence Stars"},
    "G2": {"sym": ")", "desc": "Main Sequence Stars"},
    "G5": {"sym": ")", "desc": "Main Sequence Stars"},
    "K0": {"sym": ")", "desc": "Main Sequence Stars"},
    "K5": {"sym": ")", "desc": "Main Sequence Stars"},
    "M0": {"sym": ")", "desc": "Main Sequence Stars"},
    "M5": {"sym": ")", "desc": "Main Sequence Stars"},
    "M8": {"sym": ")", "desc": "Main Sequence Stars"},
    "B0": {"sym": "]", "desc": "Giant Stars"},
    "B5": {"sym": "]", "desc": "Giant Stars"},
    "A0": {"sym": "]", "desc": "Giant Stars"},
    "F0": {"sym": "]", "desc": "Giant Stars"},
    "G0": {"sym": "]", "desc": "Giant Stars"},
    "G5": {"sym": "]", "desc": "Giant Stars"},
    "K0": {"sym": "]", "desc": "Giant Stars"},
    "K5": {"sym": "]", "desc": "Giant Stars"},
    "M0": {"sym": "]", "desc": "Giant Stars"},
    "M5": {"sym": "]", "desc": "Giant Stars"},
    "B0": {"sym": "}", "desc": "Supergiant Stars"},
    "A0": {"sym": "}", "desc": "Supergiant Stars"},
    "O5": {"sym": "}", "desc": "Supergiant Stars"},
    "F0": {"sym": "}", "desc": "Supergiant Stars"},
    "G0": {"sym": "}", "desc": "Supergiant Stars"},
    "G5": {"sym": "}", "desc": "Supergiant Stars"},
    "K0": {"sym": "}", "desc": "Supergiant Stars"},
    "K5": {"sym": "}", "desc": "Supergiant Stars"},
    "M0": {"sym": "}", "desc": "Supergiant Stars"},
    "smbh": {"sym": "@", "desc": "Black Hole"},
    "imbh": {"sym": "@", "desc": "Black Hole"},
    "sg": {"sym": "})", "desc": "Supergiant Star"},
    "star": {"sym": ")", "desc": "star"},
    "wd": {"sym": "D", "desc": "White Dwarf"},
    "bh": {"sym": "@", "desc": "Black Hole"},
    "sbh": {"sym": "@", "desc": "Black Hole"},
    "ns": {"sym": "%", "desc": "Neutron Star"},
    "sns": {"sym": "%", "desc": "Stellar Neutron Star"},
    "planet": {"sym": "o", "desc": "Planet"},
    "bd": {"sym": "b", "desc": "Brown Dwarf"},
    "ab": {"sym": "#", "desc": "Asteroid Belt"},
    "gab": {"sym": "#", "desc": "Generic Asteroid Belt"},
    "moon": {"sym": ".", "desc": "Moon"},
    "ring": {"sym": "/", "desc": "Planetary Ring"},
    "dB0": {"sym": "D", "desc": "White Dwarf"},
    "dA0": {"sym": "D", "desc": "White Dwarf"},
    "dF0": {"sym": "D", "desc": "White Dwarf"},
    "dK0": {"sym": "D", "desc": "White Dwarf"},
    "dM0": {"sym": "D", "desc": "White Dwarf"}
}
