#!/usr/bin/env python3
import logging
import os
import time

from asciimatics.widgets import Frame, ListBox, Layout, Divider, Text, \
    Button, TextBox, Widget
from asciimatics.screen import Screen, ManagedScreen
from asciimatics.scene import Scene
from asciimatics.effects import Cycle, Stars, Print
from asciimatics.renderers import FigletText, ColourImageFile, ImageFile, StaticRenderer

from .. import client
#from client import tilesets


class asciiclient(client.Client):
    def __init__(self, server=None, game=None):
        # init screen
        super().__init__(server, game)
        self.screen = Screen.open()
        self.scenes = []
#        self.show_logo()

    def __del__(self):
        self.screen.close()

    def screen_init(self, screen):
        return screen

    def clear_scenes(self):
        self.scenes = []

    def add_scene(self, effects, dur=0):
        self.scenes.append(Scene(effects, dur))

    def play(self, dur=0):
        self.screen.play(self.scenes, dur, repeat=False),

    def show_logo(self):
        # get logo from server
        payload = self.call(self.server.logo)
        logo = "".join(payload)
        effect = [
            Print(self.screen, FigletText("Welcome to"), self.screen.height // 3 - 3,
                  colour=Screen.COLOUR_BLUE),
            Print(self.screen,
                  StaticRenderer(images=[logo]),
                  self.screen.height // 3 + 5,
                  colour=Screen.COLOUR_CYAN),
            Stars(self.screen,self.screen.width+self.screen.height//2)
            ]

        self.add_scene(effect)
        self.play(100)

    def load_game(self):
        pass

    def mainmenu(self):
        ok = True
        frame = Frame(self.screen, 80,20, has_border=False)
        layout = Layout([1,1,1])
        frame.add_layout(layout)
        layout.add_widget(Button("load", self.load_game), 0)
        frame.fix()

    def mainloop(self):
        self.mainmenu()
#        self.show_logo()
        pass


if __name__ == "__main__":
    c = asciiclient()
