class Event():
    def __init__(self, client, key, func, *args):
        self.key = key
        self.func = func
        self.client = client
        self.args = args

    def activate(self):
        self.client.events[self.key] = self
#        print (self.client.events)

    def deactivate(self):
        del self.client.events[self.key]

    def trigger(self):
#        self.client.list_events()
#        print (self)
        return self.func(*self.args)

    def __repr__(self):
        #return (str(self.key) + str(self.func))
        return (str(self.key) + "(" + chr(self.key) + ") " + str(self.func.__name__))
