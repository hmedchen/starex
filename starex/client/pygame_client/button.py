#from client import event
#import event
from . import event


class Button:
    '''
    Button class. This will display a button on a given positionself.
    can be 'pressed' via mouse or highlighted char.
    Will execute a function on press
    '''

    def __init__(self, client, window, align, text, char, func, offset):
        ''' register event '''
        self.client = client
        self.window = window
        self.align = align
        self.text = text
        self.char = char
        self.func = func
        self.show_button = True
        self.offset = offset

#        print (self.char, self.func, self.align)
        self.event = event.Event(self.client, self.char, self.func)

        if self.show_button:
            self.display()
    # find dimensions of surf, positioning the button (including fixed 'frame')
    # render text on button, highlighting CHAR
    # registering event handlers for CHAR

#    def __repr__(self):
#        print ("Button ", self.char)

    def register_event(self):
        self.show_button=True
        self.event.activate()

    def unregister_event(self):
        self.show_button=False
        self.event.deactivate()

    def display(self):
#        self.event.activate()
        if self.show_button:
            if self.char == 27:
                shortcut = "ESC"
            else:
                shortcut = chr(self.char)
#            displayed = ("[{:d}] {:s}]".format((self.char), self.text))
            displayed = ("[{:s}: {:s}]".format(shortcut, self.text))
#            coords = self.window.get_coord(displayed, self.align[0], self.align[1])
#            print ("displaying button:",displayed, coords)

            self.window.add_text(displayed, self.align, self.offset) #top/left
