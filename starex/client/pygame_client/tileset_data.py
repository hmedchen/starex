TILESET_UNDEFINED = -1
TILESET_GFX = 0
TILESET_FONT = 1

charpos = {
    # -- INTERPUNCTATION
    "!": (2, 1),
    "?": (3, 15),
    ".": (2, 14),
    ",": (3, 12),
    ";": (4, 11),
    "-": (2, 13),
    "_": (5, 15),
    " ": (0, 0),
    "'": (2, 7),
    "\"": (2, 2),
    # -- BOX drawing characters -
    # see https://en.wikipedia.org/wiki/Box-drawing_character
    # - single
    "│": (11, 3),
    "─": (12, 4),
    "┌": (13, 10),
    "┐": (11, 15),
    "└": (12, 0),
    "┘": (13, 9),
    "├": (12, 3),
    "┤": (11, 4),
    "┬": (12, 2),
    "┴": (12, 1),
    "┼": (12, 5),
    # - double
    "║": (11, 10),
    "═": (12, 13),
    "╔": (12, 9),
    "╗": (11, 11),
    "╚": (12, 8),
    "╝": (11, 12),
    "╠": (12, 12),
    "╣": (11, 9),
    "╦": (12, 11),
    "╩": (12, 10),
    "╬": (12, 14),
    "-none-": (0, 0)
}

frames = {
    'FRAME_SINGLE': {
        "ltop": "┌",
        "rtop": "┐",
        "lbot": "└",
        "rbot": "┘",
        "top": "─",
        "bottom": "─",
        "left": "│",
        "right": "│"
    },
    'FRAME_DOUBLE': {
        "ltop": "╔",
        "rtop": "╗",
        "lbot": "╚",
        "rbot": "╝",
        "top": "═",
        "bottom": "═",
        "left": "║",
        "right": "║"
    },
    'FRAME_TOP': {
        "ltop": "└",
        "rtop": "┘",
        "lbot": "-none-",
        "rbot": "-none-",
        "top": "─",
        "bottom": "-none-",
        "left": "-none-",
        "right": "-none-"
    },
    'FRAME_NONE': {
        "ltop": "-none-",
        "rtop": "-none-",
        "lbot": "-none-",
        "rbot": "-none-",
        "top": "-none-",
        "bottom": "-none-",
        "left": "-none-",
        "right": "-none-"
    }
}

# FontFullPath=pygame.font.match_font('bitstreamverasans')
TTFSymbols = ("!", "?", ".", ",", ";", "-", "_", " ", "'", "\"", "\\",
                   "/", "|", ":", "[", "]", "<", ">")
