import logging
import xmlrpc.client
import json
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT']="hide"
import pygame
import sys
import time

from . import window
from . import screens
from . import event
from . import tilesets

from ..client import Client
from ... import configreader

class GFXClient(Client):
    def __init__(self, server=None, game=None):
        self.logger = logging.getLogger("Starex")
        super().__init__(server, game)
        self.configreader = configreader.ConfigReader(
            os.path.join("cfg","client"),"client.cfg")
        self.config = self.configreader.config
        self.config['basepath'] = os.path.abspath(os.curdir)

        self.game=game

#        pygame.display.init()
        pygame.init()

        self.disp = None
        self._init_display()

        self.tilesets = self._init_tilesets()

        self._init_windows()
        self.p = 0
        self.framecount = 0
        self.events = {}

#        self.quitevent = event.Event(self,pygame.K_q,self.exit)
#        self.quitevent.activate()

        self.screens = {}

    def __del__(self):
        pygame.quit()
#        self.events.a

    def toggle_scr(self, scrname):
        self.screens[scrname].toggle_screen

    def list_events(self):
        print ("v--listed events--")
        for key, event in self.events.items():
            print(key, event)
        print ("^-----------------")

    def _init_display(self):
        pygame.display.set_caption("Starex")
        pygame.display.set_icon(
            pygame.image.load(
             os.path.join(self.config['basepath'], "resource", "starex_logo.png")))
        self.disp = pygame.display.set_mode(
            (self.config['window']['size'][0]
             * self.config['tilesets']['size'][0],
             self.config['window']['size'][1]
             * self.config['tilesets']['size'][1]),
            pygame.RESIZABLE | pygame.DOUBLEBUF)

    def _init_tilesets(self):
        self.logger.debug("Init Tilesets")

        ts_file = os.path.join(self.config['basepath'],
                               self.config['tilesets']['path'],
                               self.config['tilesets']['file'])
        self.gfx_tileset = tilesets.Tileset(self.config, self.logger)
        self.gfx_tileset.LoadTileset(ts_file)

        font_file = os.path.join(self.config['basepath'],
                                 self.config['font']['path'],
                                 self.config['font']['file'])
        self.font_tileset = tilesets.Tileset(self.config, self.logger)
        self.font_tileset.LoadTTF(font_file, self.config['font']['size'])

        return [self.gfx_tileset, self.font_tileset]


    def _event_handler(self):
        # timer for server.mainfunc()
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                self.game.exit(0, 'Goodbye. (Window closed)')
                #self.exit
                #if self.game is not None:
                #    self.game.exit(0, 'Goodbye. (Window closed)')
            elif e.type == pygame.KEYDOWN:
                if e.key in self.events:
                    self.events[e.key].trigger()
            elif e.type == pygame.MOUSEBUTTONDOWN:
                pass
            elif e.type == pygame.VIDEORESIZE:  # always resize to a full tile
                req_w = int(e.size[0])
                req_h = int(e.size[1])
                req_wtiles = (req_w // self.config['tilesets']['size'][0])
                req_htiles = (req_h // self.config['tilesets']['size'][1])

                # allow only the set minimum size
                self.config['window']['size'][0] = max(
                    req_wtiles, self.config['window']['min'][0])

                # allow only the set minimum size
                self.config['window']['size'][1] = max(
                    req_htiles, self.config['window']['min'][1])

#                self.disp = pygame.display.set_mode(
#                    (200, 200), pygame.RESIZABLE)
                self.disp = pygame.display.set_mode(
                    (self.config['window']['size'][0]
                     * self.config['tilesets']['size'][0],
                     self.config['window']['size'][1]
                     * self.config['tilesets']['size'][1]),
                    pygame.RESIZABLE | pygame.DOUBLEBUF)
                self.windows.adjust_sizes()
                self.windows.display()
                pygame.display.flip()
                print(
                    "Resized to " + str(self.config['window']['size'][0])
                    + " cols, " + str(self.config['window']['size'][1])
                    + " rows.")
            # for the desired frame count - interrupt and paint
            elif e.type == pygame.USEREVENT + 1:
                self.windows.display()
                pygame.display.flip()
                self.framecount += 1
            elif e.type == pygame.USEREVENT + 2:
                if self.p == ".":
                    self.p = " "
                else:
                    self.p = "."
#                self.windows.windows['fps']['obj'].clear_text()
#                self.windows.windows['fps']['obj'].add_text(
#                        "FPS: " + str(self.framecount) + self.p, [7,1])
                self.framecount = 0

    def exit(self, code=0, msg="Goodbye."):
        #print ("exit", code, msg)
        pygame.quit()
        if self.game is not None:
            self.game.exit(code, msg)

    def _init_windows(self):
#        fps_win = window.TWindow(
#             "fps", (15, 3), window.ALIGN_X_LEFT, window.ALIGN_Y_BOTTOM,
#             'FRAME_SINGLE', fgcol=(0, 255, 255),
#             bgcol=(255, 0, 0), tilesets=self.tilesets, title='FPS')

        self.windows = window.TWindowManager(self.config, self.disp)
#        self.windows.add_window(fps_win,
#                                 [window.ALIGN_X_LEFT, window.ALIGN_Y_BOTTOM])

    def register_screens(self):
        self.screens.update(
            {'exitscreen': screens.ExitScreen("exitscreen", self, [30, 20],
                                              title="Exit?",
                                              bgcol=(133,33,33))})

        self.screens.update(
            {'newgalaxy': screens.NewGalaxy("newgalaxy", self, [30, 20],
                                              title="Create New",
                                              bgcol=(50,50,50))})

        self.screens.update(
            {'startscreen': screens.StartScreen("startscreen", self, [60,40],
                                                title="Welcome to",
                                                bgcol=(33,33,33),
                                                xalign=window.ALIGN_X_CENTER,
                                                yalign=window.ALIGN_Y_CENTER,
                                                frametype='FRAME_SINGLE')})

    def mainloop(self):
        # regular display refreshes
        pygame.time.set_timer(pygame.USEREVENT+1, 1000//self.config['fps']['maxfps'])
        pygame.time.set_timer(pygame.USEREVENT+2, 1000)

        # registering all the screens
        self.register_screens()
        # main loop, event handling
        while True:
            self._event_handler()

    def newgalaxy(self):
        self.logger.info("--- Create Galaxy ---")
        payload = self.call(self.server.create_galaxy, 2000)
        created = True
        if payload is not False:
            self.logger.debug("Done. # stars: " + str(payload['num_stars']) +
                              ", # of resolved collusions: " +
                              str(payload['num_collisions']))
        else:
            print("ERROR: Call failed!")
        return payload
