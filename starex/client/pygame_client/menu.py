from client import button


class MenuItem(button.Button):
    '''
    inherited from Button, with slightly different position and display
    '''

    def __init__(self, text, char, fund, *args):
        pass


class Menu():
    def __init__(self, menuitems=None):
        '''
        Create empty menu. Can add menu items later.
        Will create an appropriately sized window on a given screen position
        with the given menu items. MI will be highlighted when switched to.
        Adding event handlers (keyboard / mouse) for switching
        to the next item.
        '''
        pass

    def add_menu_item(self, menuitem):
        ''' adding a menu item to the list before displaying '''
        pass

    def __del__(self):
        ''' remove event handlers '''
        pass
