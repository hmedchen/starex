'''
This module handles the display of the pygame based client
like window management, text placement, resizing etc.
It should not contain any business logic.
'''
import os
# os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
import logging

from . import tileset_data
from ... import configreader

# WINDOW CONSTANTS
# Where to position the window to
ALIGN_Y_TOP = 0
ALIGN_Y_BOTTOM = 1
ALIGN_Y_CENTER = 2
ALIGN_Y_FULL = 3

# how to align the window
ALIGN_X_RIGHT = 6
ALIGN_X_LEFT = 7
ALIGN_X_CENTER = 8
ALIGN_X_FULL = 9

# How to draw the frame
FRAME_TOP = 1
FRAME_SINGLE = 2
FRAME_DOUBLE = 3
FRAME_NONE = 4

X = 0
Y = 1


class TWindowManager:
    '''
    Handles the pygame window and keeps track of all the
    frames (windows) within, their sizes and their content placement
    '''

    def __init__(self, config, surf):
        self.windows = {}
        self.surface = surf

        self.configreader = configreader.ConfigReader(
            os.path.join("cfg", "client"), "client.cfg")
        self.config = self.configreader.config
        self.logging = logging.getLogger("Starex")

        # in pixel
        self.tile_size = self.config['tilesets']['size']

        # in tiles
        self.target_size = self.get_size()
        # on init, cur_size = target_size
        self.cur_size = [self.target_size[0], self.target_size[1]]

    def add_window(self, win,  alignment):
        ''' Adds a window to the window manager '''
        mywin = {'obj': win, 'align': alignment, 'pos': [0, 0]}
        win.changed = True
        self.windows.update({win.name: mywin})
        self.adjust_size(mywin)
        return mywin['obj'].cur_size

    def del_window(self, win_name):
        ''' deletes a window from the window manager '''
        del self.windows[win_name]

    def display(self):
        ''' handles the display. gets called all the time '''
        self.surface.fill(pygame.Color(self.config['colors']['window_bg']))
        for win in self.windows.values():
            win['obj'].display()
            rect = (win['pos'][X] * self.tile_size[X],
                    win['pos'][Y] * self.tile_size[Y],
                    win['obj'].cur_size[X] * self.tile_size[X],
                    win['obj'].cur_size[Y] * self.tile_size[Y])
            self.surface.blit(win['obj'].surface, rect)

    def get_size(self):
        ''' gets size of screen in tiles '''
        return (self.surface.get_width() // self.tile_size[X],
                self.surface.get_height() // self.tile_size[Y])

    def adjust_sizes(self):
        ''' returns True if any of the windows changed size '''
        changed = False
        for win in self.windows.values():
            if self.adjust_size(win) is True:
                changed = True
        return changed

    def adjust_size(self, win):
        ''' adjusts the sizes of the provided window '''
        old_size = win['obj'].cur_size
        old_pos = win['align']
        window_size = self.get_size()
        win['obj'].changed = True

        # adjust size
        # align X
        if win['align'][X] in (ALIGN_X_RIGHT, ALIGN_X_LEFT, ALIGN_X_CENTER):
            win['obj'].cur_size[X] = min(win['obj'].target_size[X],
                                         window_size[X])
        elif win['align'][X] == ALIGN_X_FULL:
            win['obj'].cur_size[X] = window_size[X]
        # align Y
        if win['align'][Y] in (ALIGN_Y_TOP, ALIGN_Y_BOTTOM, ALIGN_Y_CENTER):
            win['obj'].cur_size[Y] = min(win['obj'].target_size[Y],
                                         window_size[Y])
        elif win['align'][Y] == ALIGN_Y_FULL:
            win['obj'].cur_size[Y] = window_size[Y]

        # adjust pos
        # pos X
        if win['align'][X] in (ALIGN_X_LEFT, ALIGN_X_FULL):
            win['pos'][X] = 0
        elif win['align'][X] == ALIGN_X_RIGHT:
            win['pos'][X] = max(window_size[X] - win['obj'].cur_size[X],
                                0)
        elif win['align'][X] == ALIGN_X_CENTER:
            win['pos'][X] = max((window_size[X] - win['obj'].cur_size[X]) // 2,
                                0)
        # pos Y
        if win['align'][Y] in (ALIGN_Y_TOP, ALIGN_Y_FULL):
            win['pos'][Y] = 0
        elif win['align'][Y] == ALIGN_Y_BOTTOM:
            win['pos'][Y] = max(window_size[Y] - win['obj'].cur_size[Y],
                                0)
        elif win['align'][Y] == ALIGN_Y_CENTER:
            win['pos'][Y] = max((window_size[Y] - win['obj'].cur_size[Y]) // 2,
                                0)
        # return true if something changed
        if old_size != win['obj'].cur_size or old_pos != win['pos']:
            win['obj'].surface = pygame.Surface(
                [win['obj'].cur_size[X] * self.tile_size[X],
                 win['obj'].cur_size[Y] * self.tile_size[Y]])
            return True
        else:
            return False


class TWindow:
    def __init__(self, name, size, xalign, yalign, frame='FRAME_NONE',
                 fgcol=(255, 255, 0), bgcol=(0, 0, 255),
                 tilesets=False, title=""):
        self.name = name
        self.logger = logging.getLogger("Starex")
        self.FrameType = frame
        self.fgcol = fgcol
        self.bgcol = bgcol
        self.xalign = xalign
        self.yalign = yalign
        self.tilesets = tilesets
        self.configreader = configreader.ConfigReader(
            os.path.join("cfg", "client"), "client.cfg")
        self.conf = self.configreader.config
        self.winframes = tileset_data.frames
        self.tile_size = self.conf['tilesets']['size']

        self.changed = False

        # will be adjusted with every resize
        self.target_size = [size[X], size[Y]]
        self.cur_size = [self.target_size[X], self.target_size[Y]]

        # stays all the time
        self.title = title
        self.text = []
        self.textpos = []
        self.textalign = []
        self.textoffset = []
        self.textwrap = False

        # why creating a surface as big as the parent win?
        self.surface = pygame.Surface([self.cur_size[X] * self.tile_size[X],
                                       self.cur_size[Y] * self.tile_size[Y]])

        self.logger.debug("TWindow class created")

    def setTile(self, char, pos, TileSet, blend=pygame.BLEND_RGBA_ADD):
        self.changed = True
        self.surface.fill(
            self.bgcol,
            (pos[X] * self.tile_size[X], pos[Y] * self.tile_size[Y],
             self.tile_size[X], self.tile_size[Y]))

        fgtile = pygame.Surface((self.tile_size[X], self.tile_size[Y]))

        fgtile.fill(self.fgcol, (0, 0, self.tile_size[X], self.tile_size[Y]))
        fgtile.blit(
            TileSet.get_tile(char),  # source image
            (0, 0, self.tile_size[X], self.tile_size[Y]),
            (0, 0, self.tile_size[X], self.tile_size[Y]), pygame.BLEND_RGBA_MIN)

        self.surface.blit(
            fgtile,  # source image
            (pos[X]*self.tile_size[X], pos[Y]*self.tile_size[Y]),  # target
            (0, 0, self.tile_size[X], self.tile_size[Y]), blend)

    def frame(self):
        tiles = self.winframes[self.FrameType]
        # paint the frame
        self.setTile(tiles["ltop"], (0, 0),
                     self.tilesets[tileset_data.TILESET_GFX])
        self.setTile(tiles["rtop"], (self.cur_size[X]-1, 0),
                     self.tilesets[tileset_data.TILESET_GFX])
        self.setTile(tiles["rbot"], (self.cur_size[X]-1, self.cur_size[Y]-1),
                     self.tilesets[tileset_data.TILESET_GFX])
        self.setTile(tiles["lbot"], (0, self.cur_size[Y]-1),
                     self.tilesets[tileset_data.TILESET_GFX])
        for col in range(1, self.cur_size[X]-1):
            self.setTile(tiles["top"], (col, 0),
                         self.tilesets[tileset_data.TILESET_GFX])
            self.setTile(tiles["bottom"], (col, self.cur_size[Y]-1),
                         self.tilesets[tileset_data.TILESET_GFX])
        for row in range(1, self.cur_size[Y]-1):
            self.setTile(tiles["left"], (0, row),
                         self.tilesets[tileset_data.TILESET_GFX])
            self.setTile(tiles["right"], (self.cur_size[X]-1, row),
                         self.tilesets[tileset_data.TILESET_GFX])
        # adding title on top
        if self.title != '':
            tmp_title = self.title
            if (len(self.title) > self.cur_size[X] - 4):
                tmp_title = self.title[:self.cur_size[X]-6] + ".."
            self.surface.fill(self.bgcol,
                              (2 * self.tile_size[X],
                               0,
                               len(tmp_title) * self.tile_size[X],
                               self.tile_size[Y]))
            for txtpos in range(len(tmp_title)):
                self.setTile(
                    tmp_title[txtpos],
                    (txtpos + 2, 0),
                    self.tilesets[tileset_data.TILESET_FONT],
                    pygame.BLEND_ADD)

    def display(self):
        # fill with bg
        if self.changed:
            self.surface.fill(self.bgcol)
            # draw frame and title
            self.frame()
            # draw text
            self.update_textpos()
            self.display_text()
            self.changed = False

    def get_size(self):
        return self.cur_size

    def get_coord(self, lines, xalign=ALIGN_X_CENTER, yalign=ALIGN_Y_CENTER):
        text_w = 0
        if type(lines) is str:
            lines = lines.split("\n")

        # text height
        text_h = len(lines)
        # longest line
        for line in lines:
            text_w = max(text_w, len(line))

        if xalign == ALIGN_X_LEFT:
            x = 0
        elif xalign in (ALIGN_X_CENTER, ALIGN_X_FULL):
            x = self.cur_size[0]//2 - (text_w)//2
        elif xalign == ALIGN_X_RIGHT:
            x = self.cur_size[0] - 2 - text_w

        if yalign == ALIGN_Y_TOP:
            y = 0
        elif yalign in (ALIGN_Y_CENTER, ALIGN_Y_FULL):
            y = self.cur_size[1]//2 - text_h//2-2
        elif yalign == ALIGN_Y_BOTTOM:
            y = self.cur_size[1] - 3

        return [x, y]

    def clear_text(self):
        self.changed = True
        self.textpos = []
        self.textalign = []
        self.textoffset = []
        self.text = []

    def update_textpos(self):
        for i in range(len(self.textpos)):
            # print (self.textpos)
            self.textpos[i] = self.get_coord(self.text[i],
                                             self.textalign[i][0],
                                             self.textalign[i][1])
            self.textpos[i][0] += self.textoffset[i][0]
            self.textpos[i][1] += self.textoffset[i][1]
#            print (self.text[i], self.textpos[i])

    def add_text(self, lineStrings, align, offset=[0, 0], wrap=False):
        self.changed = True
        self.textpos.append([0, 0])
        self.textoffset.append(offset)
        self.textalign.append(align)
        self.textwrap = wrap
        if isinstance(lineStrings, list) or isinstance(lineStrings, tuple):
            self.text.append(lineStrings[:])
        else:
            self.text.append([lineStrings])
        self.update_textpos()

    def display_text(self):
        maxchars = self.cur_size[X] - 2
        maxlines = self.cur_size[Y] - 2
        output_lines = ['' for line in range(maxlines)]

        for entry in range(len(self.text)):
            # for each line of text
            linenum = 0
            lastline = min(len(self.text[entry])+self.textpos[entry][1],
                           maxlines)
            for i in range(self.textpos[entry][1], lastline):
                if self.textwrap is True:
                    self.logger.error("Textwrap not supported yet. Continuing without.")
                # If text lines don't fit into window, cut.
                if len(self.text[entry][linenum]) <= maxchars:
                    display_line = self.text[entry][linenum]
                else:
                    display_line = self.text[entry][linenum][:maxchars - 2] + ".."
                # if there is already something on this line, merge
                if len(output_lines[i]) != 0:
                    old = [c for c in output_lines[i]]
                    new = [" " * self.textpos[entry][0]]+[c for c in display_line]
                    newline = [" " for a in range(max(len(old), len(new)))]
                    newline[:len(old)] = old[:]
                    newline[self.textpos[entry][0]:self.textpos[entry][0]+len(new)] = new
                    output_lines[i] = "".join(newline)
                else:
                    output_lines[i] = " "*self.textpos[entry][0] + display_line
                linenum += 1

        for i in range(maxlines):
            # blit text
            self.surface.fill(
                self.bgcol,
                (self.tile_size[X], (i + 1) * self.tile_size[Y],
                 len(output_lines[i]) * self.tile_size[X],
                 self.tile_size[Y]))
            for txtpos in range(len(output_lines[i])):
                self.setTile(
                    output_lines[i][txtpos],
                    (txtpos + 1,
                     i + 1),
                    self.tilesets[tileset_data.TILESET_FONT],
                    pygame.BLEND_ADD)
