#from client import event
#from client import window
#drom client import button

from . import event, window, button

import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame

class Screen(object):
    def __init__(self, name, client, size,
            xalign=window.ALIGN_X_CENTER, yalign=window.ALIGN_Y_CENTER, title = '',
            fgcol=(200,200,200), bgcol=(33,33,33), frametype='FRAME_NONE'):
        self.client = client
        self.buttons = []
        self.xalign = xalign
        self.yalign = yalign
        self.size = size
        self.title = title
        self.name = name
        self.fgcol = fgcol
        self.bgcol = bgcol
        self.frametype = frametype
        self.show = False
        self.win = None

        self.load_screen()

    def __del__(self):
        for mybutton in self.buttons:
            del mybutton

    def load_screen(self):
        self.win = window.TWindow(
            self.name, self.size, self.xalign, self.yalign,
            self.frametype, self.fgcol, self.bgcol,
            tilesets=self.client.tilesets, title=self.title)

    def toggle_screen(self):
        self.show ^= True

        if self.show:
            for mybutton in self.buttons:
                mybutton.register_event()
            self.win.cur_size = self.client.windows.add_window(self.win, [self.xalign, self.yalign])

        else:
            for mybutton in self.buttons:
                mybutton.unregister_event()
            self.client.windows.del_window(self.name)

        self.display_content()

    def display_content(self):
        pass

    def add_button(self, name, key, func, align=[7,1], offset=[0,0]):
        b=button.Button(self.client, self.win, align, name, key, func, offset)
        self.buttons.append(b)


class StartScreen(Screen):
    def __init__(self, name, client, size,
            xalign=window.ALIGN_X_CENTER, yalign=window.ALIGN_Y_CENTER, title = '',
            fgcol=(200,200,200), bgcol=(33,33,33), frametype='FRAME_NONE'):

        self.logo = []

        super().__init__(name, client, size, xalign=xalign, yalign=yalign, title=title,
                         fgcol=fgcol, bgcol=bgcol, frametype=frametype)

        # load screen is implicit
        self.toggle_screen()

#        self.event = event.Event(self.client, pygame.K_ESCAPE, client.screens['exitscreen'].toggle_screen)
#        self.event.activate()

    def load_screen(self):
        self._get_logo()

        super().load_screen()

        [x,y] = self.win.get_coord(self.logo,
                                        xalign=self.xalign,
                                        yalign=self.yalign)
        self.win.add_text(self.logo, [window.ALIGN_X_CENTER,window.ALIGN_Y_CENTER])

        self.add_button("Exit game",    pygame.K_x, self.client.screens['exitscreen'].toggle_screen,
                        (window.ALIGN_X_CENTER, window.ALIGN_Y_BOTTOM))

        self.add_button("Create New Galaxy", pygame.K_n, self.client.screens['newgalaxy'].toggle_screen,
                        (window.ALIGN_X_CENTER, window.ALIGN_Y_BOTTOM),(0,-2))

    def _get_logo(self):
        payload = self.client.call(self.client.server.logo)
        for line in payload:
            self.logo.append(line.rstrip())


class ExitScreen (Screen):
    def load_screen(self):
        super().load_screen()

        self.win.add_text("Exit game?", [window.ALIGN_X_CENTER,window.ALIGN_Y_CENTER])


        self.add_button("QUIT", pygame.K_q, self.client.exit,
                        (window.ALIGN_X_RIGHT, window.ALIGN_Y_BOTTOM))
        self.add_button("BACK", pygame.K_ESCAPE, self.toggle_screen,
                        (window.ALIGN_X_LEFT, window.ALIGN_Y_BOTTOM))

class NewGalaxy (Screen):
    def load_screen(self):
        super().load_screen()

        self.win.add_text("Create New Galaxy?",
                          [window.ALIGN_X_CENTER,window.ALIGN_Y_CENTER], (0,-2))

        self.add_button("YES", pygame.K_y, self.create_new,
                        (window.ALIGN_X_RIGHT, window.ALIGN_Y_BOTTOM))
        self.add_button("BACK"  , pygame.K_ESCAPE, self.toggle_screen,
                        (window.ALIGN_X_LEFT, window.ALIGN_Y_BOTTOM))

    def create_new(self):
        payload = self.client.newgalaxy()
        self.win.add_text(["Galaxy created.",
                            str(payload["num_stars"]) + " Stars with " +
                            str(payload["num_collisions"]) + " collisions"],
                          [window.ALIGN_X_CENTER, window.ALIGN_Y_CENTER],[0,2])

        print (payload)
