# losely based on http://usingpython.com/pygame-tilemaps/

import os
# this has to be there to prevent the stupid pygame welcome message
#os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import pygame
from pygame.locals import BLEND_ADD
#import client
#from client import client_config
from . import tileset_data
#from .. import client_config


class Tileset:
    def __init__(self, config, logger):
#        self.config = client_config.clientgfx
        self.logger = logger
        self.chars = tileset_data.charpos
        self.tileset_type = tileset_data.TILESET_UNDEFINED
        self.config = config

        self.gfx_tileset = pygame.image.load(
            os.path.join(config['basepath'],
                         self.config['tilesets']['path'],
                         self.config['tilesets']['file']))

        self.surf = pygame.Surface((
            self.gfx_tileset.get_width(),
            self.gfx_tileset.get_height()))

    def blit_char(self, c, pos, font):
        ''' BLIT the font onto a tileset '''
        # Create temp surface
        tmp_surf = pygame.Surface(self.config['tilesets']['size'])
        tmp_surf.fill(pygame.Color(0, 0, 0))

        # put TTF character onto the surface
#        print(font.size(c))
        text = font.render(
            c, True, pygame.Color(255, 255, 255))
        tmp_surf.blit(text,
                      ((self.config['tilesets']['size'][1] - text.get_width())
                       // 2 + self.config['font']['offset'][1],
                       (self.config['tilesets']['size'][0] - text.get_height())
                       // 2 + self.config['font']['offset'][0]))

        # put surfact into big surface
        # Create surface for tile only- so color can be changed
        self.surf.blit(tmp_surf,
                       (pos[1] * self.config['tilesets']['size'][0],
                        pos[0] * self.config['tilesets']['size'][1],
                        self.config['tilesets']['size'][0],
                        self.config['tilesets']['size'][1]))

        # TODO: continue here!
    def get_tile(self, char):
        # Create surface for tile only- so color can be changed
        # create a 16x16 empty surface
        single_tile_surf = pygame.Surface(self.config['tilesets']['size'])

        single_tile_surf.blit(
            self.surf,  # source image
            (0, 0),     # destination
            (self.chars[char][1] * self.config['tilesets']['size'][0],
             self.chars[char][0] * self.config['tilesets']['size'][1],
             self.config['tilesets']['size'][0],
             self.config['tilesets']['size'][1]),
            special_flags=BLEND_ADD
            )
        return (single_tile_surf)

    def set_bgcol(self, col):
        self.config['colors']['bg'] = col

    def set_fgcol(self, col):
        self.config['colors']['fg'] = col

    def _load_chars_ttf(self, font, upper_x, startchar):
        # -- UPPERCASE CHARS
        upper_y = 0
        for i in range(26):
            x = upper_x
            y = upper_y + i
            if (y > (self.config['tilesets']['grid'][0] - 1)):  # for multiline
                y = y % self.config['tilesets']['grid'][0]      # cont. on col 0
                x = upper_x + ((i + 1) // self.config['tilesets']['grid'][1])
            self.chars[chr(ord(startchar)+i)] = (x, y)
            self.blit_char(chr(ord(startchar) + i), (x, y), font)

    def LoadTTF(self, FontFullPath, FontSize):
        self.tileset_type = tileset_data.TILESET_FONT
#        try:
        font = pygame.font.Font(FontFullPath, FontSize)
#        except:
#            font = pygame.font.Font(pygame.font.match_font('sans'),
#                                    FontSize + 4)
        font.set_bold(True)

        # -- NUMBERS
        for i in range(10):
            self.chars[str(i)] = (0, i)
            self.blit_char(str(i), self.chars[str(i)], font)

        # -- UPPER-/ lowercase chars
        self._load_chars_ttf(font, 1, 'A')
        self._load_chars_ttf(font, 3, 'a')

        # -- INTERPUNCTATION
        j = 0
        for c in tileset_data.TTFSymbols:
            self.chars[c] = (5, j)
            # print(self.chars[c], c)
            self.blit_char(c, (5, j), font)
            j += 1

        pygame.image.save(self.surf, os.path.join(
            self.config['basepath'],
            self.config["font"]["path"],
            "font_tileset.png"))

    def LoadTileset(self, TilesetFullPath):
        self.tileset_type = tileset_data.TILESET_GFX
        self.surf = pygame.image.load(TilesetFullPath).convert_alpha()

        # -- NUMBERS
        for i in range(10):
            self.chars[str(i)] = (3, i)

        # -- UPPERCASE CHARS
        upper_x = 4
        upper_y = 1
        for i in range(26):
            x = upper_x
            y = upper_y + i
            if (y > (self.config['tilesets']['grid'][1] - 1)):
                y = y % self.config['tilesets']['grid'][1]
                x = upper_x + ((i + 1) / self.config['tilesets']['grid'][0])
            self.chars[chr(ord('A')+i)] = (x, y)
        # -- LOWERCASE CHARS
        lower_x = 6
        lower_y = 1
        for i in range(26):
            x = lower_x
            y = lower_y + i
            if (y > (self.config['tilesets']['grid'][1] - 1)):
                y = y % self.config['tilesets']['grid'][1]
                x = lower_x + ((i + 1) / self.config['tilesets']['grid'][0])
            self.chars[chr(ord('a')+i)] = (x, y)
        # -- Special chars1
        lower_x = 2
        lower_y = 1
        for i in range(15):
            x = lower_x
            y = lower_y + i
            if (y > (self.config['tilesets']['grid'][1] - 1)):
                y = y % self.config['tilesets']['grid'][1]
                x = lower_x + ((i + 1) / self.config['tilesets']['grid'][0])
            self.chars[chr(ord('!')+i)] = (x, y)
