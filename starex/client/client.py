import logging
import xmlrpc.client
import json
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT']="hide"
import pygame
import sys
import time

from .. import configreader

#from . import client_config

rpc_request = {
    'auth': {
        'user': None,
        'pwd': None},
    'payload': None}


class Client:
    def __init__(self, server=None, game=None):
        self.request = rpc_request
#        self.gfxconf = client_config.clientgfx
#        self.logger = logger
        self.logger = logging.getLogger("Starex")

        #self.configreader = configreader.ConfigReader(os.path.join("cfg","client"),"client.cfg")
        #self.config = client_config.config
        #self.config = self.configreader.config

        self.game = game
        if server is None:
            self.request['auth']['user'] = 'helmut'
            self.request['auth']['user'] = 'secret'
            self.server = xmlrpc.client.ServerProxy(
                self.config['SERVER_URI'])
        else:
            self.server = server
        self.windows = None
        self.logowin = None
        #self.config['basepath'] = os.path.abspath(os.curdir)

    def call(self, method, payload=None):
        self.request['payload'] = payload
        reply = method(json.dumps(self.request))
        #print("reply:",reply)
        reply = json.loads(reply)
        #print("reply:",reply)
        if reply['success']:
            return reply['payload']
        else:
            return False

    def mainloop(self):
        # a few variables to have everything to change at the same place
        save_name = "galaxy2"
        num_stars = 200000

        created = False
        ''' running some small ltests '''
        print("--- Print logo ---")
        payload = self.call(self.server.logo)
        for line in payload:
            print(line.rstrip())

        print("--- Print saved galaxies ---")
        payload = self.call(self.server.list_galaxies)
        print ("Saved galaxies:")
        saves = payload["saves"]
        time_format = "%Y-%m-%d %H:%M:%S"
        for entry in saves:
            print (" - {:20s}: Created {:20s}, last updated {:20s}".
                    format(entry["name"],
                           time.strftime(time_format, time.localtime(entry["created"])),
                           time.strftime(time_format, time.localtime(entry["last updated"]))))
        print("--- Load Galaxy ---")
        payload = self.call(self.server.load_galaxy, save_name)
        print ("Payload:",payload)

#        except FileNotFoundError:
        if payload == False:
            print("could not find file. Creating new.")
            print("--- Create Galaxy ---")
            payload = self.call(self.server.create_galaxy, num_stars)
            created = True
            if payload is not False:
                print("Done. # stars:",
                    payload['num_stars'],
                    ", # of resolved collusions:",
                    payload['num_collisions'])
            else:
                print("ERROR: Call failed!")

        print("--- Get Random System ---")
        system = self.call(self.server.random_system)
        self.print_system(system)

        print("--- Get Systems in radius ---")
        pl = {'coords': system['coords'], 'range': 100}
        area = self.call(self.server.systems_in_range, pl)
        print(area)

        if created:
            print("--- Save Images ---")
            payload = self.call(self.server.save_images,save_name)
#            print("--- Save Animation ---")
#            payload = self.call(self.server.save_animation,save_name)
            print("--- Save Galaxy ---")
            payload = self.call(self.server.save_galaxy,save_name)

    def print_system(self, payload):
        outstr = ("System @ ("
                  + str(payload["coords"]["x"]) + "/"
                  + str(payload["coords"]["y"]) + "/"
                  + str(payload["coords"]["z"]) + ")"
                  + " - ecliptic of ("
                  + str(payload["ecliptic"]["a"]) + "/"
                  + str(payload["ecliptic"]["b"]) + ") degrees.\n")
        print(outstr)
        self.print_object(payload["objects"])

    def print_object(self, obj, level=0, habzone=[0, 0]):
        indent = ""
        plus = " "
        line = " "
        for i in range(level):
            indent += " "
        if level > 0:
            plus = "+"
            line = "|"
        if obj is not None:
            token = " "
            print(indent, plus, "Object:", obj["type"], " - ",
                  obj["description"], end="")
            try:
                if obj["orbit"] > habzone[0] and obj["orbit"] < habzone[1]:
                    token = "*"
                print(" - Orbit @", round(obj["orbit"], 3), "au", token)
            except KeyError:
                print("")
            print(indent, line, "Mass:", round(obj["mass"], 3),
                  "temperature:", obj["temp"])
            if obj["lum"] > 0:
                print(indent, line,
                      "luminosity:", obj["lum"],
                      "habitable zone: [", round(obj["hab_zone"][0], 3),
                      round(obj["hab_zone"][1], 3), "]")
                habzone = obj["hab_zone"][:]
            try:
                if obj['objects']:
                    print(indent, "  Objects:")
                    for o in obj['objects']:
                        self.print_object(o, level + 2)
            except KeyError:
                pass
            if obj["satellites"] != []:
                print(indent, "  Satellites:")
                for o in obj["satellites"]:
                    self.print_object(o, level + 2, habzone)
        else:
            print("Nothing found at this location...")
