import math

galaxy_conf = {
    "dimensions": {
        "num_stars": 50000,
        "diameter": 10000,   # expected 'real' value
        "thickness": 200
    },
    "arms": {
        "rad_len": 2 * math.pi,  # how long an arm spirals
        "inner_r": 800,
        "outer_r": 150,
        "elipse_factor": 0.3
    }
}

system_conf = {
    "defaults": {
        "luminosity": 100,
        "color": "#AAFFAA"
    },
    "double_sys": {
        "min_dist": 0.5,  # in au
        "max_dist": 200
    },
    "constants": {
        "max_orbit_limit": 0.0002,   # maximum orbit distance
        "min_orbit": 0.001,  # starting at fraction of inner hab_zone
        "hab_zone_range": [0.9, 2.5],   # min and max hab zone
        "au": 149597870691,  # astronomical unit = many meters
        "sun_mass": 1.989 * math.pow(10, 30),
        "earth_mass": 5.97219 * math.pow(10, 24),
        "grav_const": 6.67384 * math.pow(10, -11),
        "sm_in_em": 332946,
        "jm_in_em": 317,  # jovian masses in earch masses
        "em_in_mm": 80,   # earth mass in moon mass
        "orbit_betavariate_val": [1.5, 5]  # betavar a,b to determine orbit rad
    }
}


# Minimum mass relationship between suns an planets
# MIN_STAR_PLANET_RELATION=98
# Schwarzschild rad=2.95km*mass(in solar masses)
# SCHWARZSCHILD_RAD_MULTIPLIER=2.95 # * Mass (mS)
# min. 80% of the mass needs to be in the center. Very optimistic
# MAX_MASS_CENTER_SATELITES_RELATION=80
# Gauss curve with 80 as outer limit? 95 (estimate) as avg?
