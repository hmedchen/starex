import random
import json
import os


class SystemProbabilities:
    def __init__(self, datadir):
        '''
        This class creates object arrays for number and types
        of stellar objects.
        While this takes more time and space to create
        (especially if the probability goes into tiny fractions),
        it will be much quicker to get a random number of
        objects or a random type, as this will be done with a
        single random.choice() call.
        '''
        self.prob = {'huge': {'num': {}, 'types': {}},
                     'big': {'num': {}, 'types': {}},
                     'medium': {'num': {}, 'types': {}},
                     'small': {'num': {}, 'types': {}},
                     'ds-stype': {'num': {}},
                     'ds-ptype': {'num': {}}}
        self.variants = {'huge': {},
                         'big': {},
                         'medium': {},
                         'small': {}}
        self.stellar_data = []
        self.datadir = datadir
        self.read_stellar_data()
        for size in ('huge', 'big', 'medium'):
            # num of objects to create
            self.create_prob_nums(size)
            # a random big object type, based on the probs
            self.create_prob_types(size)
            # a random big object, based on type and probs
            self.create_prob_objects(size)
        for type in ('ds-stype', 'ds-ptype'):
            self.create_prob_nums(type)

    def create_prob_nums(self, size):
        ''' create a number of probabilities for obj numbers '''
        try:
            prob_data = self.stellar_data[size]
        except KeyError:
            prob_data = []
            return False
        prob_list = []

        for i in range(len(prob_data['num_probs'])):
            prob_list.extend([i] * prob_data['num_probs'][i])
        self.prob[size]['num'] = prob_list
        return True

    def create_prob_types(self, size):
        ''' create a number of probabilities for obj types '''
        try:
            prob_data = self.stellar_data[size]
        except KeyError:
            prob_data = []
            return False
        prob_list = []

        # TODO: if 'prob' is an array, create one list per array entry (range)
        for value in prob_data['types'].values():
            prob_list.extend([value for i in range(value['prob'])])
        self.prob[size]['types'] = prob_list
        return True

    def create_prob_objects(self, size):
        ''' create probabily array for each object type of
        given size '''
        for type in self.stellar_data[size]['types']:
            if 'variants' in self.stellar_data[size]['types'][type]:
                num_series = len(
                    self.stellar_data[size]['types'][type]['types'])
                prob_arr = [[] for i in range(num_series)]
                for i in range(num_series):
                    for sobj in self.stellar_data[size]['types'][type]['types'][i]:
                        try:
                            prob_arr[i].extend(
                                sobj for i in range(sobj['prob']))
                        except KeyError:
                            print(type, sobj['description'], "has no objects.")
                self.variants[size] = {
                    type: self.stellar_data[size]['types'][type]['variants']}
                self.prob[size][type] = [[] for i in range(num_series)]
                for i in range(num_series):
                    self.prob[size][type][i] = prob_arr[i][:]
            else:
                prob_arr = []
                for sobj in self.stellar_data[size]['types'][type]['types']:
                    try:
                        prob_arr.extend(sobj for i in range(sobj['prob']))
                    except KeyError:
                        print(type, sobj['description'], "has no objects.")
                self.prob[size][type] = prob_arr[:]

    def plugin_json(self, filename):
        ''' returns a json structure from a given file '''
        star_data_file = os.path.join(self.datadir,
                                      filename)
        with open(star_data_file, "r") as fp:
            mountpoint = json.load(fp)
        return mountpoint

    def read_stellar_data(self):
        ''' combining several json files into a single data source '''
        stellar_data_file = os.path.join(self.datadir,
                                         'stellar_data.json')
        with open(stellar_data_file, "r") as fp:
            self.stellar_data = json.load(fp)
        # add json files that have been separated for maintainability
        self.stellar_data['big']['types']['star']['types'] = self.plugin_json(
            'star_data.json')
        self.stellar_data['big']['types']['wd']['types'] = self.plugin_json(
            'wd_data.json')
        self.stellar_data['huge']['types']['sg']['types'] = self.plugin_json(
            'o_star_data.json')
        self.stellar_data['medium']['types']['bd']['types'] = self.plugin_json(
            'bd_data.json')
        self.stellar_data['medium']['types']['planet']['types'][0] = self.plugin_json(
            'planet_data_near.json')
        self.stellar_data['medium']['types']['planet']['types'][1] = self.plugin_json(
            'planet_data_hz.json')
        self.stellar_data['medium']['types']['planet']['types'][2] = self.plugin_json(
            'planet_data_far.json')

    def get_random_count(self, size):
        ''' get a random count for given sized object '''
        try:
            return random.choice(self.prob[size]['num'])
        except KeyError:
            return 0

    def get_random_type(self, size, range=0):
        ''' get a random object type of given size '''
        # TODO: if range is given, return item from that range (if exists)
        try:
            return (random.choice(self.prob[size]['types'])['type'])
        except KeyError:
            return None

    def _get_range(self, centerobj, variant):
        '''
        handle all known cases of distribution variation
        default: return 0
        '''
        if centerobj is None:
            return 0
        if variant == 'hab_zone':
            # determine where the prev object was in relation to the hab zone.
            # has to be done that way because at this time, no orbit is
            # calculated yet.
            outer_hz = centerobj.get_hab_zone()[0]
            try:
                outer_orbit = centerobj.satellites[-1].orbit_rad
            except IndexError:
                outer_orbit = 0
#            print("outer HZ", outer_hz, "outer Orbit", outer_orbit)
            try:
                return outer_orbit // outer_hz
            except ZeroDivisionError:
                return 0
        else:
            return 0

    def get_random_object(self, size, type, centerobj=None):
        ''' get a random object of given size / type '''
        if type in self.variants[size]:
            range = self._get_range(centerobj, self.variants[size][type]['param'])
            series = 0
            for limit in self.variants[size][type]['limits']:
                if range > limit:
                    series += 1
                else:
                    break
            try:
                return (random.choice(self.prob[size][type][series]))
            except IndexError:
                print("Error", size, type)
        else:
            try:
                return (random.choice(self.prob[size][type]))
            except IndexError:
                print("Error", size, type)


if __name__ == "__main__":
    ''' quick unit tests - have to go to a test class later '''
    s = SystemProbabilities("../../data")
    print("---- huge ----")
    print(s.get_random_type('huge'))
    print(s.get_random_object('huge', 'sg'))
    print("---- big ----")
    print(s.get_random_count('big'))
    print(s.get_random_type('big'))
    print(s.get_random_object('big', 'wd'))
    print(s.get_random_object('big', 'star'))
    print("---- medium ----")
    print(s.get_random_count('medium'))
    print(s.get_random_type('medium'))
    print(s.get_random_object('medium', 'planet'))
    print("brown dwarves", s.get_random_object('medium', 'bd'))
