import random

from . import galaxy_config
from . import coords
from . import stellar_objects


class System:
    '''
    Describes a system with all objects and orbits within
    '''
    def __init__(self, logger, ecliptic=None):
        self.sysconfig = galaxy_config.system_conf
        self.logger = logger
        self.coords = coords.Coords()
#        self.luminosity = self.sysconfig["defaults"]["luminosity"]
#        self.color = pygame.Color(self.sysconfig["defaults"]["color"])
#        self.color = "#0000FF"
        # can be 0-2PI
        self.ecliptic = [0,0]
        # [0] = which angle the ecliptic raises
        # [1] = angle against the disc of the galaxy
        if ecliptic is None:
            self.ecliptic[0] = random.randint(0, 359)
            self.ecliptic[1] = int(random.betavariate(1, 4) * 180)
        else:
            self.ecliptic = ecliptic
        self.center_object = None

    @property
    def luminosity(self):
        try:
            return self.center_object.luminosity
        except AttributeError:
            return self.sysconfig["defaults"]["luminosity"]

    @property
    def color(self):
        try:
            return self.center_object.color
        except AttributeError:
            return self.sysconfig["defaults"]["color"]
            #return pygame.Color(self.sysconfig["defaults"]["color"])

    def __len__(self):
        return len(self.coords)

    def __getitem__(self, i):
        return self.coords.coord[i]

    def __repr__(self):
        return 'sys@{}'.format(self.coords)

    def set_coords(self, coords):
        ''' sets the coords from a list '''
        self.coords.x = coords[0]
        self.coords.y = coords[1]
        self.coords.z = coords[2]

    def dump(self):
         ''' saves to json '''
         co = self.center_object.dump()
         sysinfo = {'coords':
                    {'x': self.coords.x,
                     'y': self.coords.y,
                     'z': self.coords.z},
                    'lum': self.luminosity,
                    'color': self.color,
                    #'color': [self.color.r, self.color.g, self.color.b],
                    'ecliptic':
                    {'a': self.ecliptic[0],
                     'b': self.ecliptic[1]},
                    'objects': co
                    }
         return sysinfo

    def get_lum(self):
        '''
        this should return a good representation of the system objects'
        combined luminosity
        '''
        return self.luminosity

    def get_color(self):
        '''
        this should return a good representation of the system objects'
        combined color
        '''
        return self.color

    def get_obj_from_data(self, size, data):
        '''
        returns a StellarObject object from retrieved (random) data
        Using a lot of strings here - might have to reconsider that
        '''
        try:
            data['mass']
        except KeyError:
            data['mass'] = random.randint(int(data['mass_min']),
                                          int(data['mass_max']))
        if size == 'huge':
            return stellar_objects.HugeObject(self.sysconfig, data)
        elif size == 'big':
            return stellar_objects.BigObject(self.sysconfig, data)
        elif size == 'medium':
            return stellar_objects.MedObject(self.sysconfig, data)
        else:
            return stellar_objects.StellarObject(self.sysconfig, data)

    def add_med_objects(self, probs, centerobj, num_med_objects):
        for med_obj in range(num_med_objects):
            obj_data = probs.get_random_object(
                'medium', probs.get_random_type('medium'), centerobj)
            self.center_object.add_satellite(
                self.get_obj_from_data('medium', obj_data),
                centerobj,
                num_med_objects)

    def add_objects(self, probs):
        '''
        Adding objects to the system, based on the probabilities
        '''
        num_hu = probs.get_random_count('huge')
        if num_hu == 1:
            obj_data = probs.get_random_object('huge',
                                               probs.get_random_type('huge'))
            self.center_object = self.get_obj_from_data('huge', obj_data)
            return True

        num_bo = probs.get_random_count('big')
        if num_bo == 1:
            # big object
            # calculate easy orbit
            obj_data = probs.get_random_object('big',
                                               probs.get_random_type('big'))
            self.center_object = self.get_obj_from_data('big', obj_data)

            # medium objects
            num_med_objects = probs.get_random_count('medium')
            # print("num planets:", num_med_objects)
            self.add_med_objects(probs, self.center_object, num_med_objects)

        elif num_bo == 2:
            # calculate difficult orbit
            obj1 = self.get_obj_from_data(
                'big',
                probs.get_random_object('big', probs.get_random_type('big')))
            obj2 = self.get_obj_from_data(
                'big',
                probs.get_random_object('big', probs.get_random_type('big')))
            self.center_object = stellar_objects.DoubleOrbitCenter(
                self.sysconfig, obj1, obj2)

            num_med_objects = probs.get_random_count('ds-stype')
            # center objects
            self.add_med_objects(probs, self.center_object, num_med_objects)

            max_orbit = self.center_object.get_max_ptype_rad()
            # for each big object
            for obj in self.center_object.objects:
                obj.max_orbit = max_orbit
                num_med_objects = probs.get_random_count('ds-ptype')
                # center objects
                self.add_med_objects(probs, obj, num_med_objects)

        elif num_bo == 0:
            # max 2 planets, currently only one
            objdata = probs.get_random_object('medium',
                                              probs.get_random_type('medium'))
            self.center_object = self.get_obj_from_data('medium', objdata)
        else:
            # multiple big objects, chaotic system
            sobj_list = []
            for i in range(num_bo):
                objdata = probs.get_random_object('big',
                                                  probs.get_random_type('big'))
                obj = self.get_obj_from_data('big', objdata)
                sobj_list.append(obj)
            self.center_object = stellar_objects.UnorderedOrbitCenter(
                self.sysconfig, sobj_list)
