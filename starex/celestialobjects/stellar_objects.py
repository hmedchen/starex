import math
#import pygame
import random

OBJ_UNKNOWN = -1
OBJ_MASSIVE = 0
OBJ_HUGE = 1
OBJ_BIG = 2
OBJ_MED = 3
OBJ_SMALL = 4

MIN = 0
MAX = 1


class StellarObject:
    '''
    Parent class for all Stellar objects.
    '''
    def __init__(self, config, obj_detail):
        ''' constructor '''
        self.category = OBJ_UNKNOWN
        self.const = config['constants']
        self.description = obj_detail['description']
        self.orbit_rad = 0
        self.min_orb_time = 0
        self.max_orbit = 0
        try:
            self.radius = obj_detail['radius']
        except KeyError:
            self.radius = 0

        try:
            self.mass = obj_detail['mass']
        except KeyError:
            self.mass = random.uniform(obj_detail['mass_min'],
                                     obj_detail['mass_max'])
        try:
            self.type = obj_detail['type']
        except KeyError:
            self.type = ""

        try:
            self.luminosity = obj_detail['luminosity']
        except KeyError:
            self.luminosity = 0

        try:
            self.temperature = obj_detail['temperature']
        except KeyError:
            self.temperature = 0

        try:
            self.color = obj_detail['color_code']
            #self.color = pygame.Color(obj_detail['color_code'])
        except KeyError:
            self.color = "#000000"
            #self.color = pygame.Color("#000000")

        # see getter function
        self.hab_zone = [0, 0]
        self.satellites = []
        self.max_orbit = self.get_max_orbit()

    def mass_in_kg(self):
        ''' return mass of object in kg (should be rarely used) '''
        if self.category <= OBJ_BIG:
            return self.mass * self.const['sun_mass']
        else:
            return self.mass * self.const['earch_mass']

    def mass_in_ms(self):
        ''' return mass of object in sun masses (used for big+ objects) '''
        if self.category <= OBJ_BIG:
            return self.mass
        else:
            return self.mass * self.const['sm_in_em']

    def mass_in_me(self):
        ''' return mass of object in earch masses (used for med- objects) '''
        if self.category <= OBJ_BIG:
            return self.mass // self.const['sm_in_em']
        else:
            return self.mass

    def get_hab_zone(self):
        ''' setting inner/outer boundary of the habitable zone in au '''
        return [math.sqrt(self.luminosity/1.1),
                math.sqrt(self.luminosity/0.53)]

    def load(self, jsondata):
        print ( "blabla")
    #def dump(self):
#        ''' json dumps Object info - incomplete '''
#        satellites = [s.dump() for s in self.satellites]
#        objinfo = {'type': self.type,
#                   'description': self.description,
#                   'orbit': self.orbit_rad,
#                   'category': self.category,
#                   'mass': self.mass,
#                   'lum': self.luminosity,
#                   'hab_zone': self.get_hab_zone(),
#                   'temp': self.temperature,
#                   'color': [self.color.r, self.color.g, self.color.b],
#                   'satellites': satellites}
        pass

    def get_orbit_range(self, satellite):
        ''' getting min and max orbit, depending on satellite mass '''
        # alternative (less accurate):
        #   use a 'fixed' mass for the min orbit calculation,
        #   like 10me and a longer (10*) min radius time.
        #   with that, larger objects should mostly be accurate, but smaller
        #   objects would be slightly further away
        #   (but 1 day min orbit time is insanely fast anyway)
        # using self.mass without conversion, as it's native unit is needed
        self.min_orb_time = self.const.min_orbit_time * self.mass
        orbit_range = [0, 0]
        orbit_range[MIN] = math.pow(
            ((self.min_orb_time**2)*self.const['grav_const']*(
             self.mass*satellite.mass)) / (
             4*math.pi*math.pi), 0.33333) / self.const.au
        return orbit_range

#    def calc_habzone(self):
#        self.hab_zone[MAX] = math.sqrt(self.luminosity/1.1)
#        self.hab_zone[MIN] = math.sqrt(self.luminosity/0.53)

    def hill_sphere_radius(self, satellite, distance):
        '''
        gettig the radius of the hill sphere.
        (also the distance of the lagrange points L1/3 form the big object)
        '''
        if (self.mass > satellite.mass):
            return(math.pow(distance, 3) *
                   math.pow((satellite.mass / (3*self.mass)), 1/3))
        else:
            return (False)

    def dump(self):
        ''' json dumps Object info - incomplete '''
        satellites = [s.dump() for s in self.satellites]
        objinfo = {'type': self.type,
                   'description': self.description,
                   'orbit': self.orbit_rad,
                   'category': self.category,
                   'mass': self.mass,
                   'lum': self.luminosity,
                   'hab_zone': self.get_hab_zone(),
                   'temp': self.temperature,
                   'color': self.color,
                   #'color': [self.color.r, self.color.g, self.color.b],
                   'satellites': satellites}
        # see getter function
        # self.hab_zone = [0, 0]
        # self.satellites = []
        return objinfo

    def get_min_orbit(self, newobj):
        # if no satellite objects exist yet, calc minimum possible radius
        # otherwise, return most outer radius
        if len(self.satellites) == 0:
            return math.pow(
                ((self.min_orb_time**2)*self.const['grav_const']*(
                 self.mass * newobj.mass)) / (
                 4*math.pi*math.pi), 0.33333) / self.const['au']
        else:
            return self.satellites[-1].orbit_rad

    def get_max_orbit(self):
        return math.sqrt(self.mass/self.const['max_orbit_limit'])

    def add_satellite(self, sobj, center_object, num_objects):
        # calculate temperature based on sun lum and distance
        min_orbit = center_object.get_min_orbit(sobj)
        # this seem to give much better results, compacting the first,
        # stretching out the last objects
        range = ((center_object.max_orbit - min_orbit)
                 / ((num_objects - len(center_object.satellites)) ** 2))
        sobj.orbit_rad = min_orbit + (
            random.betavariate(self.const["orbit_betavariate_val"][0],
                               self.const["orbit_betavariate_val"][1]) * range)
        center_object.satellites.append(sobj)
        # print(sobj.description, sobj.orbit_rad)


class DoubleOrbitCenter (StellarObject):
    '''
    Object marking the center of a double orbit (e.g. 2 suns)
    Explicitly containing 2 Stellar Object Instances for the real masses
    '''
    def __init__(self, config, obj1, obj2):
        '''
        constructor. not using the super() constructor at all
        '''
        self.ds_probs = config['double_sys']

        self.config = config
        self.const = config['constants']
        self.category = OBJ_HUGE
        self.objects = [obj1, obj2]
        self.max_orbit = 0

        self.description = 'Double System'
        self.type = 'ds'
        self.radius = 0
        self.satellites = []

        self.mass = obj1.mass + obj2.mass
        self.luminosity = obj1.luminosity + obj2.luminosity
        self.temperature = obj1.temperature + obj2.temperature

        self.color = self.calc_color(obj1, obj2)
        #self.color = obj1.color + obj2.color
        #self.color

        self.max_orbit = self.get_max_orbit()
        # calc the orbit radius for both stars
        min_orbit = self.ds_probs['min_dist']
        range = (self.ds_probs['max_dist'] - self.ds_probs['min_dist'])
        self.orbit_rad = min_orbit + (
            random.betavariate(self.const["orbit_betavariate_val"][0],
                               self.const["orbit_betavariate_val"][1]) * range)

        self.objects[0].orbit_rad = self.orbit_rad / (1 + (
                                                      self.objects[0].mass /
                                                      self.objects[1].mass))
        self.objects[1].orbit_rad = self.orbit_rad / (1 + (
                                                      self.objects[1].mass /
                                                      self.objects[0].mass))

    def calc_color(self, obj1, obj2):
        c_all = [0,0,0]

        #if one object it dark, return other color
        if not hasattr(obj1, "luminosity")  or obj1.luminosity == 0:
            return obj2.color
        elif not hasattr(obj2, "luminotisy") or obj2.luminotisy == 0:
            return obj1.color

        lum_diff = obj1.luminosity / obj2.luminosity
        c1 = [int(obj1.color[1:3],16), int(obj1.color[3:5],16), int(obj1.color[5:7],16)]
        c2 = [int(obj2.color[1:3],16), int(obj2.color[3:5],16), int(obj2.color[5:7],16)]
        try:
            c_all = [int(c1[0]+c2[0]/lum_diff),
                    int(c1[1]+c2[1]/lum_diff),
                    int(c1[2]+c2[2]/lum_diff)]
        except ZeroDivisionError:
            print(obj1.luminosity, obj2.luminosity)
        maxval = max(c_all)
        if maxval > 255:
            for i in range(len(c_all)):
                c_all[i] = int(c_all[i] * (255/maxval))
#        c_all = [min(int(c1[0]*lum_diff)+c2[0], 255),
#                min(int(c1[1]*lum_diff)+c2[1], 255),
#                min(int(c1[2]*lum_diff)+c2[2], 255)]
#        print (c1, obj1.luminosity, c2,obj2.luminosity,
#                "diff:",lum_diff, c_all, max(c_all))

        return "#{0:02x}{1:02x}{2:02x}.format(c_all[0],c_all[1],c_all[2])"

    def dump(self):
        satellites = [s.dump() for s in self.satellites]
        cobjects = [s.dump() for s in self.objects]
        objinfo = {'type': self.type,
                   'description': self.description,
                   'category': self.category,
                   'mass': self.mass,
                   'hab_zone': self.get_hab_zone(),
                   'lum': self.luminosity,
                   'temp': self.temperature,
                   'color': self.color,
                   #'color': [self.color.r, self.color.g, self.color.b],
                   'objects': cobjects,
                   'satellites': satellites}
        return objinfo

    def get_max_ptype_rad(self):
        return 0.2 * abs(self.objects[0].orbit_rad
                         - self.objects[1].orbit_rad)

    def get_min_orbit(self, bigobj=None):
        return 3 * self.orbit_rad


class UnorderedOrbitCenter (StellarObject):
    ''' object marking the system center in an unordered system
    (>2 biggest objects)'''
    def __init__(self, config, objlist):
        ''' constructor. not usin gthe Super() constructor '''
        if type(objlist) != list:
            raise TypeError('Passed objects need to be in a list')
        self.config = config
        self.const = config['constants']
        self.category = OBJ_HUGE
        self.objects = objlist
        self.type = 'uoc',
        self.description = 'Multiple System without determinable orbits'
        self.radius = 0
        self.mass = 0
        self.luminosity = 0
        self.temperature = 0
        #self.color = pygame.Color(0, 0, 0)
        self.color = "#000000"

        for obj in objlist:
            self.mass += obj.mass
            self.luminosity += obj.luminosity
            self.temperature += obj.temperature
            #self.color += obj.color
        self.color = "#ffff00"

    def dump(self):
        ''' json dumps Object info - incomplete '''
        cobjects = [s.dump() for s in self.objects]
        objinfo = {'type': self.type,
                   'description': self.description,
                   'category': self.category,
                   'mass': self.mass,
                   'hab_zone': self.get_hab_zone(),
                   'lum': self.luminosity,
                   'temp': self.temperature,
                   #'color': [self.color.r, self.color.g, self.color.b],
                   'color': self.color,
                   'satellites': cobjects}
        # see getter function
        # self.hab_zone = [0, 0]
        # self.satellites = []
        return objinfo


class HugeObject (StellarObject):
    ''' Huge object. if one of them exists, nothing else can exist in a sys '''
    def __init__(self, config, obj_detail):
        super().__init__(config, obj_detail)
        self.category = OBJ_HUGE


class BigObject (StellarObject):
    ''' big objects. Suns and similar '''
    def __init__(self, config, obj_detail):
        super().__init__(config, obj_detail)
        self.category = OBJ_BIG


class MedObject (StellarObject):
    ''' med objects, planets and similar '''
    def __init__(self, config, obj_detail):
        super().__init__(config, obj_detail)
        self.category = OBJ_MED
