import random
import math
import time
import json
import os
import pickle

from . import galaxy_config
from . import coords
from . import system
from . import system_probabilities

from .kdtree import kdtree


class Galaxy:
    ''' main Galaxy Object '''
    def __init__(self, config, logger):
        '''
        Galaxy constructor.
        Also inits a probability instance, which might take time
        '''
        self.config = config
        self.galaxy_conf = galaxy_config.galaxy_conf
        self.logger = logger
        self.systems = []
        self.num_systems = 0
        self.rand_seed = None
        self.coord_hash = dict()
        self.num_collisions = 0
        self.tree = None
        self.fname_meta = "galaxy.meta"
        self.fname_pickle = "galaxy.dat"
        self.fname_json = "galaxy.json"
        pconfig = os.path.join(self.config['basepath'],
                               self.config['locations']['datadir'])

        self.probs = system_probabilities.SystemProbabilities(pconfig)

    def create_sys_objects(self):
        ''' Creating the system content for each system in the Galaxy '''
        for sys in self.systems:
            sys.add_objects(self.probs)

    def tree_create(self):
        ''' create the kdtree. this can take a while '''
        self.logger.debug("creating kdtree..")
        self.tree = kdtree.create(self.systems)
        self.logger.debug("creating kdtree done.")

    def print(self):
        ''' deprecated I guess... not much sense to do that with 100000 sys '''
        print(self.systems)

    def load(self, dirname):
        self.logger.debug("loading Galaxy..")

        #if one of the files it not there, error and exit.
        if (not os.path.isfile(os.path.join(dirname,self.fname_meta))
        or not os.path.isfile(os.path.join(dirname,self.fname_pickle))):
            self.logger.error("Cannot load Galaxy. file not found")
            return False

        with open(os.path.join(dirname,self.fname_meta), "r") as fp:
            load_data = json.load(fp)
        with open(os.path.join(dirname,self.fname_pickle), "rb") as fp:
#                pickle.dump(self.systems, fp)
            self.systems = pickle.load(fp)

#        with open(filename, "rb") as fp:
#            load_data = pickle.load(fp)
        self.rand_seed = load_data['rand_seed']
        self.num_systems = load_data['num_systems']
#        self.systems = load_data['systems']
        # cannot load tree, as it doesn't pickle
        self.tree_create()

        self.logger.debug("loading Galaxy done.")
        return True

    def save(self, dirname):
        ''' dumps complete galaxy to json file '''
        self.logger.debug("saving Galaxy..")

        # cannot store kdtree as it doesn't pickle :(
        save_metadata= {'file_version': 2,
                    'num_systems': self.num_systems,
                    'rand_seed': self.rand_seed}
        try:
            # saving metadata
            print (save_metadata)
            with open(os.path.join(dirname,self.fname_meta), "w") as fp:
                json.dump(save_metadata, fp)
                # saving pickled data
            with open(os.path.join(dirname,self.fname_pickle), "wb") as fp:
                #            pickle.dump(save_data, fp)
                pickle.dump(self.systems, fp)
            with open(os.path.join(dirname,self.fname_json), "w") as fp:
                #for sys in [s.dump() for s in self.systems]:
#                for sys in [s.dump() for s in self.systems]:
                sysinfo = [{'coords': {'x': sys.coords.x, 'y': sys.coords.y, 'z': sys.coords.z},
                            'lum': sys.luminosity, 'color': sys.color} for sys in self.systems]
                json.dump(sysinfo, fp)
        except:
            self.logger.error("Cannot save galaxy!")
            raise

        self.logger.debug("saving Galaxy done.")

    def clear(self):
        ''' clear all system data and counters, for re-creation '''
        del self.systems[:]
        self.coord_hash.clear()
        self.num_collisions = 0
        self.num_systems = 0

    def seed(self, seed):
        ''' sets random seed for galaxy creation, for repeatable creation'''
        self.rand_seed = seed
        random.seed(self.rand_seed)

    def random_system(self):
        ''' returns a random system '''
        try:
            s=random.choice(self.systems)
#            kdtree.visualize(self.tree)
            return s.dump()
        except IndexError:
            return None

    def add_disc(self, radius, thickness, num_stars):
        '''
        Creation module.
        Creates a flat disc of stars and adds it to the systems list
        '''
        systems = [system.System(self.logger) for x in range(num_stars)]
        outstr = ("Creating disc with r=" + str(radius) +
                  " h=" + str(thickness) + " n=" + str(num_stars))
        self.logger.debug(outstr)
        radius *= self.galaxy_conf['dimensions']['diameter']
        thickness *= self.galaxy_conf['dimensions']['thickness']
        starttime = time.time()
        rad = 2 * math.pi  # the angular 'rad'
        flatten = radius / thickness
        for i in range(0, num_stars):
            act_coords = coords.Coords()
            # converting Length, a, b into coords
            # radius is normalvariate, alpha, beta is random 0 < x < 360degrees
            act_coords.set_polar([random.uniform(0, rad),
                                 random.uniform(0, rad),
                                 random.normalvariate(0, radius / 2)])
            # flatten ball
            act_coords.z = int(act_coords.z // flatten)
            # check for dupes
            if self.coords_available(act_coords):
                systems[i].coords = act_coords
            else:
                # try again
                i -= 1
        self.systems.extend(systems)
        self.num_systems += len(systems)

        return (time.time() - starttime)

    def add_arms(self, radius, thickness, num_stars, num_arms):
        '''
        Creation module.
        Creates a several spiral arms of stars and adds it to the systems list
        '''
        outstr = ("Creating " + str(num_arms) + " arms with r=" + str(radius) +
                  " h=" + str(thickness) + " n=" + str(num_stars))
        self.logger.debug(outstr)
        radius *= self.galaxy_conf['dimensions']['diameter']
        thickness *= self.galaxy_conf['dimensions']['thickness']
        starttime = time.time()
        rad = 2 * math.pi  # the angular 'rad'

        # how long do the arms spiral around the center
        arms_max_rad = self.galaxy_conf['arms']['rad_len']
        # stars per arm
        stars_per_arm = num_stars // num_arms

        for arm_num in range(num_arms):
            arm_angle = 0
            arm_start_rad = arms_max_rad / num_arms * arm_num
            systems = [system.System(self.logger)
                       for x in range(stars_per_arm)]
            for i in range(stars_per_arm):
                act_coords = coords.Coords()
                random_sphere = coords.Coords()
                # set where on the way to the edge we are
                act_a = arm_angle + arm_start_rad
                act_b = 0
                act_L = radius / stars_per_arm * i
                act_coords.set_polar([act_a, act_b, act_L])
                # get random "ball" with a given radius for the star
                # percent of radius * differnce
                rand_sphere_rad = (
                    (1 - act_L / radius) *
                    (self.galaxy_conf['arms']['inner_r'] -
                     self.galaxy_conf['arms']['outer_r'])
                    ) + self.galaxy_conf['arms']['outer_r']
                random_sphere.set_polar(
                    [random.uniform(0, rad),
                     random.uniform(0, rad),
                     random.normalvariate(0, rand_sphere_rad)])
                # flatten random ball
                random_sphere.z = int(random_sphere.z *
                                      self.galaxy_conf['arms']['elipse_factor'])
                act_coords += random_sphere
                # check for dupes
                if self.coords_available(act_coords):
                    systems[i].coords = act_coords
                else:
                    # try again
                    i -= 1

                # update arm angle for the next arm
                arm_angle += (arms_max_rad / num_stars) * num_arms
            self.systems.extend(systems)
            self.num_systems += len(systems)

        return (time.time() - starttime)

    def coords_available(self, coords):
        ''' Hash from coordinates to make a quick check that a spot is not
        occupied twice '''
        tmp_key = str(coords.x) + "-" + str(coords.y) + "-" + str(coords.z)
        if tmp_key in self.coord_hash:
            self.logger.debug("Dupe coords detected @" + str(coords))
            self.num_collisions += 1
            return False
        else:
            self.coord_hash[tmp_key] = True
            return True
