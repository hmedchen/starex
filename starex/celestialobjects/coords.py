import math

# simple constants for dimension reference
X = 0
Y = 1
Z = 2


class Coords:
    ''' Class with a simple [x,y,z] list and corresponding functions '''
    def __init__(self, coords=[0, 0, 0]):
        self.coord = coords[:]

    def __add__(self, a):
        ''' overload of '+' '''
        x = self.x + a.x
        y = self.y + a.y
        z = self.z + a.z
        return Coords([x, y, z])

    def __sub__(self, a):
        ''' overload of '-' '''
        x = self.x - a.x
        y = self.y - a.y
        z = self.z - a.z
        return Coords([x, y, z])

    def __str__(self):
        ''' overload of str(x), used mainly for print() '''
        printstr = ("(" + str(self.coord[X]) + " / " +
                    str(self.coord[Y]) + " / " + str(self.coord[Z]) + ")")
        return printstr

    def __len__(self):
        ''' overload of len(s) - always 3 '''
        return 3

    def __getitem__(self, i):
        ''' overload of Coords[i] '''
        return self.coord[i]

    @property
    def x(self):
        ''' getter for coords.x '''
        return self.coord[X]

    @x.setter
    def x(self, value):
        ''' setter for coords.x '''
        self.coord[X] = value

    @property
    def y(self):
        ''' getter for coords.y '''
        return self.coord[Y]

    @y.setter
    def y(self, value):
        ''' setter for coords.y '''
        self.coord[Y] = value

    @property
    def z(self):
        ''' getter for coords.z '''
        return self.coord[Z]

    @z.setter
    def z(self, value):
        ''' setter for coords.z '''
        self.coord[Z] = value

    def get_coords(self):
        ''' retrieving the coord list [x,y,z] '''
        return self.coord

    def set_polar(self, polar_coords):
        ''' setting the coords according to given polar coords [a,b,L] '''
        if type(polar_coords) is list:
            a, b, L = polar_coords
        else:
            return False
        len_xy = L * math.cos(b)
        self.coord[Z] = int(L * math.sin(b))
        self.coord[X] = int(len_xy * math.cos(a))
        self.coord[Y] = int(len_xy * math.sin(a))
        return True

    def get_polar(self):
        ''' getting polar coords [a,b,L] of current position '''
        len_xy = math.sqrt(self.coord[X] * self.coord[X] +
                           self.coord[Y] * self.coord[Y])
        a = math.atan2(self.coord[Y], self.coord[X])
        b = math.atan2(self.coord[Z], len_xy)
        L = math.sqrt(len_xy * len_xy + self.coord[Z] * self.coord[Z])
        return [a, b, L]
