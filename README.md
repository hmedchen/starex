# STAREX
A Star Exploration Game.

```
     ________________ ____ _________   ____    
    /  ______   ___ //    \\_______ \ / ___\   
    \__\     | |    / /||\ \  ___   \ \ /       
        ___  | |   / /_||_\ \ \__\   \ \        
  ______\  \ | |__/ ___  __ \_______/ \ \______
 /_________/ |_____/   ||  \_________/ \_____
```

## Experience
Starex is a roguelike 4x game, focusing on exploration and with a simplistic frontend.
The player has a whole Galaxy to explore, with procedurally generated systems, planets and NPCs (Non-Player Civilizations).
The timing of the game is not fully decided yet, probably time-limited round based.
To be able to sustain attacks while the player is not online, there will be the possibility of script based runbooks triggered on timings or triggers.
The physics try to stay as realistic as possible (keeping playability in mind), or at least offer a realistic option (ballistic weaponry, no translight communication / movement, no artificial gravity).

A vast majority of the players will be NPCs, with the possibility of a multiplayer option when run on a server.
However, the current focus is to tune the game for single player.
The NCPs will interact with each other and the player, and there should be a long history of events that were pre-calculated for each Galaxy.

## License
Starex is GNU GPLv3 (see `./LICENSE`)

## Platform
As Starex is based on python, it should be able to run universally. However, development will focus on Linux.

## Setup
Starex is strictly separated between a server and a client
 * The server is written exclusively in python for expandability reasons
 * The client is kept as 'dumb' as possible, and will be a text-based python script at the beginning.
 * the client server communication will start using a basic rpc protocol with user authentication.
 * The client will host no data that the player did not discover.

## Formatting
### Coding conventions

#### Naming
 * Class names are `CamelCase`
 * Constants use `CAPS_WITH_UNDERSCORE`
 * filenames and other names use `lowercase_with_underscore`
#### Comments
 * Source code should have sufficient documentation
 * each class and method should have a docstring
#### File layout
 * tabs are 4 characters
 * lines should be limited to 80 chars
 * runnable files should have a main block:
```
if __name__ == "__main__":
    main()
```

### Source formatting
 * The .py files are coded according to [pycodestyle] (http://pycodestyle.pycqa.org/en/latest/) [PEP 8] (https://www.python.org/dev/peps/pep-0008/)
 * current tool of choice is
   * [Atom] (https://atom.io/) Editor
     * [atom-ide-ui] (https://atom.io/packages/atom-ide-ui) package
     * [ide-python] (https://atom.io/packages/ide-python) package

### Directory structure
 * server and client separated in `starex_server` and `starex_client`
 * legacy scripts in `/.legacy`
    * these can be used as reference or copied, but not referred to / included
 * a single command in `.` as link/wrapper to the start script
 * scripts in `./bin`
 * config files in `./etc`
 * documentation in `./doc`
 * persistent databases / data files in `./data`
