#!/usr/bin/env bash
# determines if unit tests are to be run or not
run_tests=false
wait_time=0
VENV_PATH=~/venv_starex/bin/
PYTHON=python

echo "----------- starting standalone ------------"
${VENV_PATH}${PYTHON} ./starex.py $*
