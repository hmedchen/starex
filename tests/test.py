import unittest

from celestialobjects import coords

import logging
from celestialobjects import system


class CoordsTestCase(unittest.TestCase):
    ''' tests for coords class'''

    def test_input_output(self):
        ''' testing basic input/output functions '''
        c = coords.Coords([4, 5, 6])

        # testing __str__
        formatted_output = str(c)
        self.assertEqual(formatted_output, '(4 / 5 / 6)')

        # list retrieval
        self.assertEqual(c.get_coords(), [4, 5, 6])

    def test_getter_setter(self):
        ''' testing setters and getters '''
        c = coords.Coords([4, 5, 6])

        # getters
        self.assertEqual(c.x, 4)
        self.assertEqual(c.y, 5)
        self.assertEqual(c.z, 6)

        # setters
        c.x = 6
        self.assertEqual(c.x, 6)
        c.y = 7
        self.assertEqual(c.y, 7)
        c.z = 8
        self.assertEqual(c.z, 8)

    def test_arithmetics(self):
        ''' testing basic arithmetics '''
        c = coords.Coords([4, 5, 6])
        d = coords.Coords([2, 7, 1])

        add = c + d
        self.assertEqual(add.get_coords(), [6, 12, 7])

        add2 = coords.Coords([1, 1, 1])
        add2 += c
        self.assertEqual(add2.get_coords(), [5, 6, 7])

        subtr = c - d
        self.assertEqual(subtr.get_coords(), [2, -2, 5])

        subtr2 = coords.Coords([1, 1, 1])
        subtr2 -= c
        self.assertEqual(subtr2.get_coords(), [-3, -4, -5])

        length = len(c)
        self.assertEqual(length, 3)

    def test_polar(self):
        ''' testing setting and retrieving polar coords '''
        c = coords.Coords([4, 5, 6])
        polar = c.get_polar()
        self.assertAlmostEqual(polar[0], 0.8960553845713439)
        self.assertAlmostEqual(polar[1], 0.7529077706294455)
        self.assertAlmostEqual(polar[2], 8.774964387392123)

        self.assertTrue(c.set_polar([0.5, 0.2, 11]))
        self.assertFalse(c.set_polar(7))

        self.assertEqual(c.get_coords(), [9, 5, 2])


class SystemTestCase(unittest.TestCase):
    ''' tests for system objects'''

    def __init__(self, *args):
        ''' init a system, coords and logger obj '''
        super().__init__(*args)
        self.logger = logging.getLogger()
        self.mysys = system.System(self.logger)
        self.mycoords = coords.Coords([4, 5, 6])

    def test_input_output(self):
        ''' testing input and output functions '''
        self.assertEqual(str(self.mysys), "sys@(0 / 0 / 0)")
        self.assertEqual(len(self.mysys), 3)

    def test_getter_setter(self):
        ''' testing getters and setters '''
        self.mysys.set_coords(self.mycoords)
        self.assertEqual(str(self.mysys), "sys@(4 / 5 / 6)")

        self.assertEqual(self.mysys.get_lum(), 100)
        self.assertEqual(self.mysys.get_color(), (170, 255, 170, 255))

    def test_random_ecliptic(self):
        ''' testing range of the random ecliptic '''
        self.assertGreaterEqual(self.mysys.ecliptic[0], 0)
        self.assertLessEqual(self.mysys.ecliptic[0], 359)
        self.assertGreaterEqual(self.mysys.ecliptic[1], 0)
        self.assertLessEqual(self.mysys.ecliptic[1], 180)

    def test_set_ecliptic(self):
        ''' testing that the set ecliptic is ok '''
        newsys = system.System(self.logger, [10, 50])
        self.assertEqual(newsys.ecliptic[0], 10)
        self.assertEqual(newsys.ecliptic[1], 50)


if __name__ == "__main__":
    unittest.main()
